<?php

require_once 'Zend/Mail.php';

class AbuseController extends Application_MyController
{
    public function init()
    {
        parent::init();

        $this->_helper->layout()->setLayout('default');

        $this->_model = new Application_Model_Abuse();


        if (isset($_SESSION["MESSAGE"])) {
            $this->view->message = $_SESSION["MESSAGE"];
        }
        unset($_SESSION["MESSAGE"]);

        if (isset($_SESSION["POBIERZ"])) {
            $file = "/public/raports/listOfUsers.txt";

            $filename = "listOfUsers.txt";
            $type = filetype($file);
            header("Content-type: $type");
            header("Content-Disposition: attachment;filename=$filename");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: " . filesize($file));
            header('Pragma: no-cache');
            header('Expires: 0');
            set_time_limit(0);
            readfile($file);
            unset($_SESSION["POBIERZ"]);
        }
    }

    public function indexAction()
    {
        $this->_redirect("/abuse/list");
    }

    public function listAction()
    {
        $searchId = $this->prepareSearch();

        $this->_params["defaultOrder"] = "abuse_id_DESC";

        if (isset($this->_params["deleteAFilter"]) && $this->_params["deleteAFilter"] != ""
            && isset($this->_params["filterSelect"]) && $this->_params["filterSelect"] != ""
        ) {
            $this->_model->deleteFilter($this->_params["filterSelect"]);
            $this->_redirect("/" . $this->_params["controller"] . "/list");
        }

        if ($paginator = $this->_model->getAbuseList($this->_params)) {
            $abuses = $paginator->getIterator()->toArray();

            $this->view->pages = $paginator->getPages();

            $this->view->abuses = $abuses;
        } else {
            $this->escape("OPERATION_FAIL");
        }

        $this->view->listOfFilters = $this->_model->getListOfFilters($this->_params["controller"]);

    }


    public function performactionAction()
    {

        if ($this->_params["selectedAction"] == "generateRaport") {
            $this->generateRaport($this->_params["itemId"], $this->_params["controller"], $this->_model);
        }

        $itemId = $this->_params["itemId"];

        $action = $this->_params["selectedAction"];

        $i = 0;
        while (isset($itemId[$i])) {
            $this->_model->$action($itemId[$i]);
            $i = $i + 1;
        }
        $_SESSION["MESSAGE"] = "Zmiany zostały zapisane";
        $this->goBack("OPERATION_OK", 1);
    }


    public function messageAction()
    {

        $adminEmail = "piotr.proc@gmail.com";
        $this->view->adminEmail = $adminEmail;

        $user_id = $_GET["user_id"];

        $receiver = $this->_model->getUser($user_id);
        $this->view->receiver = $receiver;
    }

    public function sendMessageAction()
    {
        $userID = $_POST["recipientId"];
        $title = $_POST["title"];
        $message = $_POST["text"];

        $receiver = $this->_model->getUser($userID);
        $receiverName = $receiver["user_name"];
        $receiverEmail = $receiver["user_email"];

        $this->sendEmail($receiverName, $receiverEmail, $title, $message);
        $_SESSION["MESSAGE"] = "Mail został wysłany";
        $this->_redirect("/abuse/list");
    }

    private function sendEmail($receiverName, $receiverEmail, $title, $message)
    {

        require_once('Zend/Mail/Transport/Smtp.php');
        require_once 'Zend/Mail.php';

        $config = array('auth' => 'login',
            'username' => 'ang2e24@gmail.com',
            'password' => '2e242e24',
            'ssl' => 'tls');

        $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);

        $mail = new Zend_Mail('UTF-8');
        //$mail->setBodyText($message);
        $mail->setBodyHtml($message);
        $mail->setFrom('', 'Admin serwisu');
        $mail->addTo($receiverEmail, $receiverName);
        $mail->setSubject($title);
        $mail->send($transport);
    }


    public function convertAction()
    {

        $this->listAction();

    }

}

?>