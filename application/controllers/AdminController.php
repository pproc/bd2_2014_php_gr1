<?php


class AdminController extends Application_GridController
{
    private $model;

    public function init()
    {
        parent::init();

        $this->model = new Application_Model_Admin();

        if (isset($_SESSION["MESSAGE"])) {
            $this->view->message = $_SESSION["MESSAGE"];
        }
        unset($_SESSION["MESSAGE"]);
    }

    public function indexAction()
    {
        $this->_redirect("/admin/list");
    }

    public function listAction()
    {
        $admins = $this->model->getAll();
        $this->view->admins = $admins;
        $this->view->numberOfAdmins = count($admins);
    }


    public function addAction()
    {
        $login = $_POST["login"];
        $password_md5 = md5($_POST["password"]);

        $this->model->add($login, $password_md5);
        $_SESSION["MESSAGE"] = "Pozycja została dodana";
        $this->_redirect("/admin/list");
    }

    public function addFormAction()
    {
        $admins = $this->model->getAll();
        $this->view->admins = $admins;
    }

    public function editFormAction()
    {
        $id = $_GET['id'];

        $adminProfile = $this->model->getOne($id);
        $this->view->adminProfile = $adminProfile;
    }

    public function afterEditFormAction()
    {
        $id = $_GET['id'];
        $login = $_POST['login'];
        $is_superadmin = $_POST['is_superadmin'];

        $result = $this->model->editProfile($id, $login, $is_superadmin);
        $this->view->result = $result;

        $_SESSION["MESSAGE"] = "Zmiany zostały zapisane";
        $this->_redirect("/admin/list");
    }

    public function deleteAction()
    {
        $id = $_GET['id'];

        $this->model->delete($id);

        $_SESSION["MESSAGE"] = "Pozycja została usunięta";
        $this->_redirect("/admin/list");
    }

    public function checkIfAdminExistsAction()
    {
        $adminEmail = $_POST["adminEmail"];
        $result = $this->model->checkIfAdminExists($adminEmail);

        echo json_encode( $result );
        exit;
    }

}
