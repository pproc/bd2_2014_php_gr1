<?php
class Form_Users_Login extends Zend_Form
{
  public function init()
  {
    $login = $this->createElement('text', 'user_login');
	
    $login->setLabel('Login:')
            ->setRequired(TRUE)
            ->setAttrib('size', 30)
            ->addFilters(array(
                new Zend_Filter_StringToLower(),
                new Zend_Filter_StringTrim(),
                new Zend_Filter_StripNewlines(),
                new Zend_Filter_StripTags()
            ))
            ->addValidators(array(
                new Zend_Validate_NotEmpty()
            ));
			
    $haslo = $this->createElement('password', 'user_password');
	
    $haslo->setLabel('Has�o:')
            ->setRequired(TRUE)
            ->setAttrib('size', 30)
            ->addFilters(array(
                new Zend_Filter_StringToLower(),
                new Zend_Filter_StringTrim(),
                new Zend_Filter_StripNewlines(),
                new Zend_Filter_StripTags()
            ))
            ->addValidators(array(
                new Zend_Validate_NotEmpty()
            ));
			
    $this->addElements(array(
        $login,
        $haslo,
        array(
            'submit', 'submit', array(
                'label' => 'zaloguj'
                )
            )
        ));
  }
}
