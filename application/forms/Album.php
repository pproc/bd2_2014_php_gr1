<?php

class Application_Form_NewUser extends Zend_Form
{
    public function init()
    {
        $this->setName('newUser');

        $login = new Zend_Form_Element_Text('login');
        $login->setLabel('Login:')
              ->setRequired(true)
              ->addFilter('StripTags')
              ->addFilter('StringTrim')
              ->addValidator('NotEmpty');

        $title = new Zend_Form_Element_Text('password');
        $title->setLabel('Haslo:')
              ->setRequired(true)
              ->addFilter('StripTags')
              ->addFilter('StringTrim')
              ->addValidator('NotEmpty');

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id', 'submitbutton');

        $this->addElements(Array($artist, $title, $submit));
    }
}