<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	public function run()
	{
		/*if(isSet($_REQUEST["menu"]))
		{
			$menu = $_REQUEST["menu"];

			$requestParams = $_REQUEST;

			$router = Zend_Controller_Front::getInstance()->getRouter();

			$route = new Zend_Controller_Router_Route($menu, Array('controller' => 'page', 'action' => 'view', 'pageUrl' => $menu));

			$router->addRoute('pageView', $route);
		}*/

		$autoloader = Zend_Loader_Autoloader::getInstance();

		$autoloader->registerNamespace('Bvb_');

		$autoloader->registerNamespace('Application_');

		Zend_Loader::loadClass('Spyc');

		Zend_Loader::loadClass('VFrame');

		parent::run();
	}

	protected function _initView()
	{
		$config = new Zend_Config_Ini('../application/configs/application.ini');

		Zend_Registry::set('config', $config);

		Zend_Session::start();

		$view = new Zend_View_Smarty();

		$view->setScriptPath($config->smarty->template_dir);

		$view->setCompilePath($config->smarty->compile_dir);

		$view->setCachePath($config->smarty->cache_dir);

		$view->setConfigPath($config->smarty->config_dir);

		$view->setBasePath($config->smarty->template_dir);

		if (isset ($_SESSION['userAuth']))
			$view->assign('user',$_SESSION['userAuth']);

		if (isset ($_SESSION['userEmail']))
			$view->assign('user_email',$_SESSION['userEmail']);



		Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer')
		->setViewScriptPathSpec($_SERVER["DOCUMENT_ROOT"].'/application/views/templates/:controller/:action.:suffix')
		->setViewSuffix('tpl')
		->setView($view);

		$view->layout = Zend_Layout::startMvc(Array(
			'inflectorTarget' => $_SERVER["DOCUMENT_ROOT"].'/application/layouts/:script.:suffix',
			'layout' => 'default',
			'viewSuffix' => 'tpl'
		))->setView($view);
	}

}

