<script src="/public/scripts/views/admin/add-form.js" type="text/javascript"></script>

<div class="panel panel-primary">

    <div class="panel-heading">
        <h3 class="panel-title">
            <span class="glyphicon glyphicon-tower"></span>
            Administratorzy - nowy administrator
            <a  id="submlink"  href="javascript: history.go(-1)">
                <span class="glyphicon glyphicon-arrow-left"></span>
                Powrót
            </a>
        </h3>
    </div>

    <div class="panel-body">
        <form id="mainAdminForm" role="form" action="/admin/add" method="post" >
            <div class="form-group input-group-sm">
                <input id="login" class="form-control" name="login" type="text"  placeholder="Podaj e-mail administratora" >
                <select id="adminList" style="display:none" >
                    {foreach name=loop from=$admins item=admin}
                        <option value="{$admin.admin_login}">{$admin.admin_login}</option>
                    {/foreach}
                </select>
            </div>

            <div class="form-group input-group-sm">
                <input id="password" class="form-control" name="password" type="password"  placeholder="Podaj hasło administratora" >
            </div>

            <div class="form-group input-group-sm">
                <input id="repeatPassword" class="form-control" name="repeatPassword" type="password"  placeholder="Powtórz hasło" >
            </div>

            <div class="form-group input-group-sm">
                <select class="form-control"  name="is_superadmin" id="is_superadmin">
                    <option value="" selected>Czy dany admin powinien być super administratorem?</option>
                    <option value="1">tak</option>
                    <option value="0">nie</option>
                </select>
            </div>

            <div class="form-group input-group-sm">
                <button class="btn btn-primary" type="submit" id="submButton" >dodaj administratora</button>
            </div>

        </form>
    </div>

</div>