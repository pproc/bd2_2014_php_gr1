<script src="/public/scripts/views/admin/list.js" type="text/javascript"></script>

{include file="includes/message-to-show.tpl"}

<div class="panel panel-primary">

    <div class="panel-heading">

        <h3 class="panel-title">
            <span class="glyphicon glyphicon-tower"></span>
            Administratorzy: ilość znalezionych wierszy: {$numberOfAdmins}

            <a id="submlink" href="/admin/add-form">
                Dodaj administratora
                <span class="glyphicon glyphicon-plus-sign"></span>
            </a>
        </h3>

    </div>

    <div class="panel-body">

    {if $admins}

        <form method="post" action="/admin/performaction" id="adminActionForm" class="global-form" role="form">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">

                    <tr>
                        <th>Lp</th>
                        <th>login</th>
                        <th>hash hasła</th>
                        <th>data dodania</th>
                        <th>superadmin</th>
                        <th>edytuj profil</th>
                        <th>zmień hasło</th>
                        <th>usuń</th>
                        <th>usuń ajaxem</th>
                    </tr>

                    {foreach name=loop from=$admins item=admin}

                        <tr>
                            <td>{$smarty.foreach.loop.index + 1}</td>
                            <td>{$admin.admin_login}</td>
                            <td>{$admin.admin_password}</td>
                            <td>{$admin.admin_create_time|date_format:"%d-%m-%Y"}</td>
                            <td>{if $admin.admin_is_superadmin}tak{else}nie{/if}</td>
                            <td>
                                <a href="/admin/edit-form?id={$admin.admin_id}">edytuj</a>
                            </td>
                            <td>
                                <a href="/admin/edit-password?id={$admin.admin_id}">zmień</a>
                            </td>
                            <td>
                                <a href="/admin/delete?id={$admin.admin_id}">usuń</a>
                            </td>
                            <td>
                                <a href="#" class="delButton" id="delButton-{$admin.admin_id}">usuń</a>
                            </td>
                        </tr>

                    {/foreach}


                </table>
            </div>
        </form>

        {else}
        <div class="noDataDiv">
            <h4 class="text-warning text-center">Nie znaleziono żadnego wiersza spełniającego kryteria.</h4>
        </div>
    {/if}
    </div>

</div>