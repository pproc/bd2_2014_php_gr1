<script src="/public/scripts/views/admin/edit-profile.js" type="text/javascript"></script>

<div class="panel panel-primary">

    <div class="panel-heading">
        <h3 class="panel-title">
            <span class="glyphicon glyphicon-tower"></span>
            Administratorzy - edycja
            <a id="submlink" href="javascript: history.go(-1)">
				<span class="glyphicon glyphicon-arrow-left">
				</span>
                Powrót
            </a>
        </h3>
    </div>

    <div class="panel-body">
        ﻿{if $adminProfile}
            <form action="/admin/after-edit-form?id={$adminProfile.admin_id}" method="post" id="editAdminForm">

                <div class="form-group input-group-sm">
                    <label for="login">login: </label>
                    <input class="form-control " id="login" name="login" type="text" value='{$adminProfile.admin_login}'
                           class="wider"/>
                    <select id="adminList" style="display:none">
                        {foreach name=loop from=$admins item=admin}
                            <option value="{$admin.admin_login}">{$admin.admin_login}</option>
                        {/foreach}
                    </select>
                </div>

                <div class="form-group input-group-sm">
                    <label for="is_superadmin">super administrator: </label>
                    <select class="form-control" name="is_superadmin" id="is_superadmin">
                        <option value="1"
                                {if $adminProfile.admin_is_superadmin == "1"}selected{/if}>
                            tak
                        </option>
                        <option value="0"
                                {if $adminProfile.admin_is_superadmin == "0"}selected{/if}>
                            nie
                        </option>
                    </select>
                </div>

                <div class="form-group input-group-sm">
                    <button class="btn btn-primary" type="submit" id="submButton">zapisz</button>
                </div>

            </form>
        {else}

            <h4 class="text-warning text-center">Takiego użytkownika nie ma w bazie.</h4>

        {/if}
    </div>
</div>