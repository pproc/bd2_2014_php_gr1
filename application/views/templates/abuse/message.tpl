
{literal}
<script type="text/javascript">


    $(document).ready(function(){

        $("#textInput").val($("#messageTextDiv").html());

        $("#messageTextDiv").keydown(function(){
            $("#textInput").val($("#messageTextDiv").html());
        });

        $("#submButton").click(function(){
            $("#textInput").val($("#messageTextDiv").html());
        });

        $('#sendMessageForm').validate({
            rules:
            {
                recipientId: {
                    required: true
                },
                text: {
                    required: true
                }
            },

            onkeyup: false,

            messages:
            {
                recipientId:{
                    required: "To pole jest wymagane"
                },
                text: {
                    required: "To pole jest wymagane"
                }
            }
        });
    });

</script>
{/literal}

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">
            <span class="glyphicon glyphicon-bookmark"></span>
            Wyślij wiadomość
            <a id="submlink" style="float: right; padding-left:1%" href="javascript: history.go(-1)">
				<span class="glyphicon glyphicon-arrow-left">
				</span>
                Powrót
            </a>
        </h3>
    </div>

    <div class="panel-body">
        <form action="/abuse/send-message" method="post" class="form-horizontal" role="form" id="sendMessageForm" >

            <div class="form-group input-group-sm add-form mailInputDiv">
                <label for="recipient">Odbiorca: </label>
                <select class="form-control"  name="recipientId" class="wider" id="recipient_name_select">
                    <option value="{$receiver.user_id}" selected>
                        {$receiver.user_name} < {$receiver.user_email} >
                    </option>
                </select>
            </div>

            <div class="form-group input-group-sm add-form mailInputDiv">
                <label for="text">Tytuł maila: </label>
                <input type="text" name="title" class="form-control"/>
            </div>

            <div class="form-group input-group-sm add-form mailInputDiv">
                <label for="text">Treść pierwszej wiadomości: </label>
                <div contenteditable="true" class="form-control"  id="messageTextDiv" class="wider" style="height: 300px; width: 500px">
                    <br/> <br/> <br/>Z poważaniem <br/><b>Administrator serwisu</b>
                </div>
            </div>

            <input class="form-control mailInputDiv" id="textInput" name="text" type="text" class="wider" style="display:none" >

            <div class="form-group input-group-sm add-form mailInputDiv">
                <button class="btn btn-primary" type="submit" id="submButton" >Wyślij wiadomość</button>
            </div>

        </form>

        <div class="alertInfo" id="alertInfoEmptyFields" style="display:none; color:red">
            <h4 class="text-warning text-center">Wszystkie pola powinny być uzupełnione</h4>
        </div>

    </div>
</div>