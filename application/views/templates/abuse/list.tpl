{literal}

<link rel='stylesheet' type='text/css' href='/public/css/jquery-ui.css'/>
<script src="/public/scripts/jquery-ui.min.js"></script>

<script type="text/javascript">

    $(document).ready(function () {
        $(".selectAllCheckbox").click(function () {
            AllCheckboxToggle($(this));
        });
        $(".actionSelect").change(
                function () {
                    var value = $(this).val();
                    $(".actionSelect").val(value);
                    SubmitAction2($(this), "abuseActionForm");
                }
        );

        $("#sentFromDate").datepicker({ dateFormat:'dd-mm-yy' });
        $("#sentToDate").datepicker({ dateFormat:'dd-mm-yy' });

    });

</script>
{/literal}

{include file="includes/message-to-show.tpl"}

<div class="panel  panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">
            <span class="glyphicon glyphicon-fire"></span>
            Nadużycia

            <a id="submlink" style="float: right; padding-left:1%" href="/abuse/convert">
				<span class="glyphicon glyphicon-arrow-left">
				</span>
                Konwersja PDF
            </a>
        </h3>

    </div>

    <div class="panel-body">
        <div class="filterDiv">

            <form method="post" action="/abuse/list" id="filterForm" class="form-inline" role="form">
                <div class="form-group input-group-sm">
                    <label for="ownerNameFilter" class="sr-only">osoba zgłaszająca: </label>
                    <input class="form-control" placeholder="osoba zgłaszająca" type="text" name="ownerNameFilter"
                           id="ownerNameFilter"
                           value="{if isSet($searchParams.ownerNameFilter)}{$searchParams.ownerNameFilter}{/if}"/>
                </div>

                <div class="form-group input-group-sm">
                    <label for="userNameFilter" class="sr-only">osoba zgłaszana: </label>
                    <input placeholder="osoba zgłaszana" class="form-control" type="text" name="userNameFilter"
                           id="userNameFilter"
                           value="{if isSet($searchParams.userNameFilter)}{$searchParams.userNameFilter}{/if}"/>
                </div>

                <div class="form-group input-group-sm">
                    <label class="sr-only" for="sentFromDate">wysłane od: </label>
                    <input placeholder="wysłane od" class="form-control" type="text" id="sentFromDate"
                           name="sentFromFilter"
                           value="{if isSet($searchParams.sentFromFilter)}{$searchParams.sentFromFilter}{/if}"/>
                </div>

                <div class="form-group input-group-sm">
                    <label for="sentToDate" class="sr-only">wysłane do: </label>
                    <input placeholder="wysłane do" class="form-control" type="text" id="sentToDate" name="sentToFilter"
                           value="{if isSet($searchParams.sentToFilter)}{$searchParams.sentToFilter}{/if}"/>
                </div>

                <div class="form-group input-group-sm">
                    <label for="statusFilter" class="sr-only">status zgłoszenia: </label>
                    <select class="form-control" name="statusFilter" id="statusFilter">
                        <option value="" selected>status zgłoszenia</option>
                        <option value="1"
                                {if isSet($searchParams.statusFilter) && $searchParams.statusFilter == "1"}selected{/if}>
                            Nierozpatrzone
                        </option>
                        <option value="2"
                                {if isSet($searchParams.statusFilter) && $searchParams.statusFilter == "2"}selected{/if}>
                            Zaakceptowane
                        </option>
                        <option value="3"
                                {if isSet($searchParams.statusFilter) && $searchParams.statusFilter == "3"}selected{/if}>
                            Odrzucone
                        </option>
                    </select>
                </div>

                <br/> <br/>

            {include file="includes/rows-per-page.tpl"}



                <div class="form-group input-group-sm">
                    <input class="btn btn-warning" type="button" name="clear" value="wyczyść"
                           onclick="ClearForm('filterForm');"/>
                </div>

                <div class="form-group input-group-sm">
                    <input class="btn btn-primary" type="submit" name="filter" value="filtruj"/>
                </div>

                <br/> <br/>

            {include file="includes/filter-support.tpl"}

                <br/> <br/>

            </form>

        </div>

    {if $abuses}

    {include file="includes/pagination.tpl"}


        <form method="post" action="/abuse/performaction" id="abuseActionForm" class="global-form" role="form">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                {include file="abuse/action-select.tpl"}
                    <tr>
                        <th style="text-align: center">Lp</th>
                        <th style="text-align: center">{sortlink order='DESC' column='abuse_owner_id' searchId=$searchId}
                            osoba zgłaszająca{sortlink order='ASC' column='abuse_owner_id' searchId=$searchId}</th>
                        <th style="text-align: center">{sortlink order='DESC' column='abuse_user_id' searchId=$searchId}
                            osoba zgłaszana{sortlink order='ASC' column='abuse_user_id' searchId=$searchId}</th>
                        <th style="text-align: center">{sortlink order='DESC' column='abuse_message' searchId=$searchId}
                            wiadomość{sortlink order='ASC' column='abuse_message' searchId=$searchId}</th>
                        <th style="text-align: center">{sortlink order='DESC' column='abuse_status' searchId=$searchId}
                            status zgłoszenia{sortlink order='ASC' column='abuse_status' searchId=$searchId}</th>
                        <th style="text-align: center">{sortlink order='DESC' column='abuse_create_time' searchId=$searchId}
                            data zgłoszenia{sortlink order='ASC' column='abuse_create_time' searchId=$searchId}</th>
                        <th style="text-align: center">zaznacz</th>
                    </tr>

                    {foreach name=loop from=$abuses item=abuse}

                        <tr>
                            <th style="text-align: center">{$smarty.foreach.loop.index + 1}</th>
                            <td>
                                <b>
                                    {$abuse.abuse_owner_name}
                                </b>
                                <a href="/abuse/message?user_id={$abuse.abuse_owner_id}">
                                    {$abuse.abuse_owner_email}
                                </a>

                            </td>
                            <td>
                                <b>
                                    {$abuse.abuse_user_name}
                                </b>
                                <a href="/abuse/message?user_id={$abuse.abuse_user_email}">
                                    {$abuse.abuse_user_email}
                                </a>
                            </td>
                            <td>{$abuse.abuse_message}</td>
                            <td style="text-align: center">
                                {if $abuse.abuse_status eq 1}
                                    Nierozpatrzone
                                    {elseif $abuse.abuse_status eq 2}
                                    Zaakceptowane
                                    {elseif $abuse.abuse_status eq 3}
                                    Odrzucone
                                {/if}
                            </td>
                            <td style="text-align: center">{$abuse.abuse_create_time|date_format:"%d-%m-%Y"}</td>
                            <td style="text-align: center">
                                <input type="checkbox" class="itemCheckbox" name="itemId[]" value="{$abuse.abuse_id}"/>
                            </td>
                        </tr>

                    {/foreach}

                {include file="abuse/action-select.tpl"}

                </table>
            </div>
        </form>

    {include file="includes/pagination.tpl"}


        {else}
        <div class="noDataDiv">
            <h4 class="text-warning text-center">Nie znaleziono żadnego wiersza spełniającego kryteria.</h4>
        </div>
    {/if}
    </div>
</div>