<tr class="actionTr">
    <td colspan="6" class="actionTd" style="border-right: none">
        <div class="input-group">
            <label class="input-group-addon">akcja: </label>
            <select class="form-control actionSelect"  name="selectedAction" class="">
                <option value="0">-- wybierz --</option>
                <option value="delete">usuń</option>
                <option value="consider">ustaw jako rozpatrzone</option>
                <option value="dismiss">ustaw jako odrzucone</option>
                <option value="reconsider">zaznacz do ponownego rozpatrzenia</option>
                <option value="generateRaport">pobierz dane w pliku tekstowym</option>
            </select>
        </div>
    </td>
    <td  style="text-align: center">
        <input type="checkbox" class="selectAllCheckbox"/>
    </td>
</tr>