<div class="form-group input-group-sm">
    <label for="perPage">l. wierszy: </label>
    <select class="form-control"  name="perPage" id="perPage">
        <option selected> -- </option>
        <option value="5" {if isSet($searchParams.perPage) && $searchParams.perPage == "5"}selected{/if}>5</option>
        <option value="10" {if isSet($searchParams.perPage) && $searchParams.perPage == "10"}selected{/if}>10</option>
        <option value="20" {if isSet($searchParams.perPage) && $searchParams.perPage == "20"}selected{/if}>20</option>
    </select>
</div>