<div class="form-group input-group-sm">
    <label for="perPage">l. wierszy: </label>
    <select class="form-control"  name="perPage" id="perPage">
        <option selected> -- </option>
        <option value="50" {if isSet($searchParams.perPage) && $searchParams.perPage == "50"}selected{/if}>50</option>
        <option value="100" {if isSet($searchParams.perPage) && $searchParams.perPage == "100"}selected{/if}>100</option>
        <option value="200" {if isSet($searchParams.perPage) && $searchParams.perPage == "200"}selected{/if}>200</option>
    </select>
</div>