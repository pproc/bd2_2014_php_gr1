{if isset($pages->pageCount)}
<ul class="pagination">

    {if $pages->pageCount > 1}

        {if isset($pages->previous)}
            {if isSet($searchId)}
                <li><a href="{pagelink page=$pages->previous searchId=$searchId}">&laquo;</a></li>
                {else}
                <li><a href="{pagelink page=$pages->previous}">&laquo;</a></li>
            {/if}
        {/if}

        {foreach from=$pages->pagesInRange item=page}
            {if $page eq $pages->current}
                <li class="active"><a href="#">{$page}<span class="sr-only"></span></a></li>
                {else}
                {if isSet($searchId)}
                    <li><a href="{pagelink page=$page searchId=$searchId}">{$page}</a></li>
                    {else}
                    <li><a href="{pagelink page=$page}">{$page}</a></li>
                {/if}
            {/if}
        {/foreach}

        {if isset($pages->next)}
            {if isSet($searchId)}
                <li><a href="{pagelink page=$pages->next searchId=$searchId}">&raquo;</a></li>
                {else}
                <li><a href="{pagelink page=$pages->next}">&raquo;</a></li>
            {/if}
        {/if}

    {/if}

</ul>

{/if}