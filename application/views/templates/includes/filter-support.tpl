{literal}
<script type="text/javascript">

        $(document).ready(function(){

    $('#filterForm').validate({
        rules:{
            saveAFilter:{
                customvalidation:true,
            },
        },

        onkeyup:false,

        messages:{
            saveAFilter:{
                customvalidation:"Filtr o takiej nazwie już istnieje",
            },
        },
        errorPlacement:function (error, element) {
            error.insertAfter(element.next()); // default function
        },
    });

    function checkIfAlreadyExists(value) {
        var result = true;
        $('#filterSelect option').each(function () {
            if ($(this).text() == value) {
                result = false;
            }
        });
        return result;
    }

    $.validator.addMethod("customvalidation",
            function (value) {
                //alert(value);
                return checkIfAlreadyExists(value);
            }
    );

{/literal}
    var tableName = "{$searchParams.controller}";
    var actionName = "{$searchParams.action}";
{literal}

    $("#filterSelect").change(function () {

        var chosenValue = $(this).val();
        window.location.replace("/" + tableName + "/" + actionName + "/searchId/" + chosenValue);

    });

});

</script>
{/literal}
<br/>
<div class="form-group input-group-sm">
    <label for="filterSelect">filtry: </label>
    <select width="40px" name="filterSelect" id="filterSelect">
        <option value="">-- wybierz --</option>
    {foreach name=loop from=$listOfFilters item=filter}
        <option value="{$filter.filter_search_id}"
                {if isSet($searchParams.saveAFilter) && $searchParams.saveAFilter eq $filter.filter_name}selected {/if}>{$filter.filter_name}</option>
    {/foreach}
    </select>

    <input class="btn btn-warning" type="submit" name="deleteAFilter" value="usuń filtr"/>
</div>

<div class="form-group input-group-sm">
    <label style="display: none;" for="saveAFilter">
        czy zapisać filtr:
    </label>
    <input placeholder="Zapisz filtr pod nazwą..." type="text" name="saveAFilter"/>
    <input class="btn btn-primary" type="submit" name="" value="zapisz "/>
</div>

