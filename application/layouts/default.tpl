<!DOCTYPE html>
<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Expires" content="0"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Cache-Control" content="no-cache"/>
    <meta name="author" content="netsoftware.pl"/>

    <script src="/public/scripts/jquery.min.js" type="text/javascript"></script>
    <script src="/public/scripts/jquery.validate.js" type="text/javascript"></script>
    <script src="/public/scripts/jquery.validate.myrules.js" type="text/javascript"></script>
    <script src="/public/scripts/grids.js" type="text/javascript"></script>
    <script src="/public/bootstrap-3.0.3-dist/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/public/bootstrap-3.0.3-dist/dist/css/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet" href="/public/css/grid.css" type="text/css"/>
    <link rel="stylesheet" href="/public/bootstrap-3.0.3-dist/dist/css/bootstrap-theme.min.css" type="text/css"/>
    <link rel="stylesheet" href="/public/css/admin.css" type="text/css"/>

    <title>PHP, Zend Framework, MVC</title>

</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    <nav class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li>
                <a class="navbar-brand" href="#">
                    Panel administracyjny
                </a>
            </li>
        </ul>
    </nav>
</div>

<div class="container">
    <div class="row">
        <div class="sidebar col-lg-2 col-sm-12">
            <ul class="nav nav-pills nav-stacked">
                <li class="adminCategory">
                    <a href="/index/index">
                        <span class="glyphicon glyphicon-user"></span>
                        Boot Camp 1
                    </a>
                </li>
                <li class="adminCategory">
                    <a href="/admin/list">
                        <span class="glyphicon glyphicon-tower"></span>
                        Boot Camp 2
                    </a>
                </li>
                <li class="adminCategory">
                    <a href="/abuse/list">
                        <span class="glyphicon glyphicon-fire"></span>
                        Boot Camp 3
                    </a>
                </li>
                <li class="adminCategory">
                    <a href="/webservice/AddPayment.php">
                        <span class="glyphicon glyphicon-transfer"></span>
                        Web service
                    </a>
                </li>
                <li class="adminCategory">
                    <a href="/webservice/AddFacture.php">
                        <span class="glyphicon glyphicon-transfer"></span>
                        Web service 2
                    </a>
                </li>

            </ul>
        </div>

        <div class="content col-lg-10 col-sm-12">

            <div class="messageDiv">
            </div>

            {$layout->content}

        </div>

</body>

</html>

