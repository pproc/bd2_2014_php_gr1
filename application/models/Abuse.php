<?php

class Application_Model_Abuse extends Application_Model_MyModel
{

    private $_db;

    private $_abuses;

    public function __construct()
    {
        parent::__construct();

        $this->_db = Zend_Db_Table::getDefaultAdapter();

        $this->_abuses = new Application_Model_DbTable_Abuses();
    }

    public function getAbuseList($params)
    {

        if (isset($params["saveAFilter"]) && $params["saveAFilter"] != ""
            && (!isset($params["filter"]) || $params["filter"] == "")
        ) {
            $this->saveFilter($params);
        }

        $select = $this->_abuses->prepareFetchAllSelect();

        if (isset($params["ownerNameFilter"]) && $params["ownerNameFilter"] != "") {
            $select->where("ud1.user_name LIKE '%" . $params["ownerNameFilter"] . "%'
				or u1.user_email LIKE '%" . $params["ownerNameFilter"] . "%'");
        }

        if (isset($params["userNameFilter"]) && $params["userNameFilter"] != "") {
            $select->where("ud2.user_name LIKE '%" . $params["userNameFilter"] . "%'
				or u2.user_email LIKE '%" . $params["userNameFilter"] . "%'");
        }

        if (isset($params["sentFromFilter"]) && $params["sentFromFilter"] != "") {

            $date = strtotime(str_replace("/", "-", $params["sentFromFilter"]));
            $timestamp = date("Y-m-d", $date);

            $select->where("abuse_create_time > '" . $timestamp . "'");
        }

        if (isset($params["sentToFilter"]) && $params["sentToFilter"] != "") {

            $date = strtotime(str_replace("/", "-", $params["sentToFilter"]));
            $timestamp = date("Y-m-d", $date);

            $select->where("abuse_create_time < '" . $timestamp . "'");
        }

        if (isset($params["categoryFilter"]) && $params["categoryFilter"] != "") {
            $select->where("abuse_category =" . $params["categoryFilter"]);
        }

        if (isset($params["statusFilter"]) && $params["statusFilter"] != "") {
            $select->where("abuse_status =" . $params["statusFilter"]);
        }


        $select->order(Application_Utils::getOrderClause($params));

        $paginator = Application_Utils::getPaginator($select, $params);

        return $paginator;
    }

    public function delete($id)
    {
        $query = "DELETE FROM abuses
				WHERE abuse_id=" . $id;
        $result = $this->_db->query($query);
    }

    public function consider($id)
    {
        $query = "UPDATE abuses SET abuse_status = 2 WHERE abuse_id=" . $id;
        $result = $this->_db->query($query);
    }

    public function reconsider($id)
    {
        $query = "UPDATE abuses SET abuse_status = 1 WHERE abuse_id=" . $id;
        $result = $this->_db->query($query);
    }

    public function dismiss($id)
    {
        $query = "UPDATE abuses SET abuse_status = 3 WHERE abuse_id=" . $id;
        $result = $this->_db->query($query);
    }

    public function getAllAbuses()
    {
        $query = "SELECT
                a.abuse_id,
                a.abuse_category,
                a.abuse_message,
                a.abuse_status,
                a.abuse_create_time,
                ud1.user_id as abuse_owner_id,
                ud1.user_name as abuse_owner_name,
                ud2.user_id as abuse_user_id,
                ud2.user_name as abuse_user_name,
                u1.user_email as abuse_owner_email,
                u2.user_email as abuse_user_email

        FROM abuses as a
        JOIN user_details as ud1 ON a.abuse_owner_id = ud1.user_id
        JOIN user_details as ud2 ON a.abuse_user_id = ud2.user_id
        JOIN users as u1 ON a.abuse_owner_id = u1.user_id
        JOIN users as u2 ON a.abuse_user_id = u2.user_id
        ";

        $result = $this->_db->query($query);

        return $result->fetchAll();
    }

    public function getUser($user_id){

        $query = "SELECT * FROM users as u
                JOIN user_details as ud ON u.user_id = ud.user_id
                WHERE u.user_id = $user_id";

        $user = $this->_db->fetchRow($query);
        return $user;
    }

}

