<?php

class Application_Model_Index
{

    private $db;
    private $admins;

    public function __construct()
    {
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->admins = new Application_Model_DbTable_Admins();
    }

    public function getAll()
    {
        $query = "SELECT * FROM admins";
        return $this->db->fetchAll($query);
    }

    public function getOne($id)
    {
        $query = "SELECT * FROM admins
                  WHERE admin_id = $id";
        return $this->db->fetchRow($query);
    }

    public function delete($id)
    {
        $query = "DELETE FROM admins
                  WHERE admin_id = $id";

        return $this->db->query($query);
    }

    public function getAll2()
    {
        return $this->admins->fetchAll();
    }

    public function getOne2($id)
    {
        return $this->admins->fetchRow("admin_id = $id");
    }

    public function delete2($id)
    {
        $where = $this->admins->getAdapter()->quoteInto('admin_id = ?', $id);
        return $this->admins->delete($where);
    }

}

