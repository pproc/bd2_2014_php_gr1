<?php

class Application_Model_DbTable_Abuses extends Zend_Db_Table_Abstract
{
    protected $_name = 'abuses';

    public function prepareFetchAllSelect()
    {
        $select = $this->select();

        $select->from(
            array("a"=>$this->_name,
                "ud1"=>"user_details",
                "ud2"=>"user_details",
                "u1"=>"users",
                "u2"=>"users",
            ),
            array(
                "a.abuse_id",
                "a.abuse_category",
                "a.abuse_message",
                "a.abuse_status",
                "a.abuse_create_time",
                "ud1.user_id as abuse_owner_id",
                "ud1.user_name as abuse_owner_name",
                "ud2.user_id as abuse_user_id",
                "ud2.user_name as abuse_user_name",
                "u1.user_email as abuse_owner_email",
                "u2.user_email as abuse_user_email",
            )
        );

        $select->join("user_details as ud1","a.abuse_owner_id = ud1.user_id");
        $select->join("user_details as ud2","a.abuse_user_id = ud2.user_id");
        $select->join("users as u1","a.abuse_owner_id = u1.user_id");
        $select->join("users as u2","a.abuse_user_id = u2.user_id");

        $select->setIntegrityCheck(false);

        return $select;
    }
}
