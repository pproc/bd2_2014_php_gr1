<?php

class Application_Model_Admin extends Application_Model_GridModel
{
    private $db;

    public function __construct()
    {
        parent::__construct();
        $this->db = Zend_Db_Table::getDefaultAdapter();
    }

    public function getOne($id)
    {
        $query = "SELECT * FROM admins where admin_id = $id";
        $result = $this->db->fetchRow($query);
        return $result;
    }

    public function getAll()
    {
        $query = "SELECT * FROM admins";
        $result = $this->db->fetchAll($query);
        return $result;
    }

    public function add($login, $password_md5)
    {
        $query = "INSERT INTO admins(admin_login, admin_password)
				VALUES ('$login','$password_md5')";
        $result = $this->db->query($query);
        return $result;
    }

    public function delete($id)
    {
        $query = "DELETE FROM admins
				WHERE admin_id = $id";

        $result = $this->db->query($query);
        return $result;
    }

    public function changePassword($id, $previousPasswordMD5, $passwordMD5)
    {
        $query = "SELECT admin_password FROM admins WHERE admin_id = $id";
        $result = $this->db->fetchRow($query);

        if ($result["admin_password"] != $previousPasswordMD5) {
            return false;
        }

        $query = "UPDATE admins SET admin_password='$passwordMD5' WHERE admin_id=$id";
        $this->db->query($query);
        return true;
    }

    public function editProfile($id, $login, $is_superadmin)
    {
        $query = "UPDATE admins SET admin_login='$login', admin_is_superadmin = $is_superadmin
                WHERE admin_id = $id";
        $result = $this->db->query($query);
        return $result;
    }

    public function checkIfAdminExists($adminEmail)
    {
        $query = "SELECT * FROM admins WHERE admin_login = '$adminEmail'";

        $result = $this->db->fetchRow($query);

        if($result)
            return false;
        else
            return true;
    }

}

