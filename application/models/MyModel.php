<?php

class Application_Model_MyModel
{
    private $_db;

    public function __construct(){
        $this->_db = Zend_Db_Table::getDefaultAdapter();
    }

    public function saveFilter($params){
        $table_name = $params["controller"];
        $searchId = $params["searchId"];
        $filter_name = $params["saveAFilter"];

        $query = "SELECT * from filters WHERE filter_search_id =".$searchId;
        $result = $this->_db->fetchRow($query);

        if($result["filter_id"]){
        }else{
            $query = "INSERT INTO filters(filter_table, filter_search_id, filter_name)
						VALUES ('".$table_name.
                "', ".$searchId.
                ", '".$filter_name.
                "');";
            $result = $this->_db->query($query);
        }
    }

    public function getListOfFilters($controller){
        $query = "SELECT *
				FROM  filters 
				WHERE filter_table = '".$controller."'
				ORDER BY filter_name 
				";

        $result = $this->_db->fetchAll($query);
        return $result;
    }

    public function deleteFilter($searchId){
        $query = "DELETE
				FROM  filters 
				WHERE filter_search_id = ".$searchId;

        $result = $this->_db->query($query);
    }

    public function addToRaport($i, $fileName, $information){
        $data = $i.". ".$information["user_name"]." <".$information["user_email"]."> dowiedział się o stronie - ".$information["dict_value"]."\r\n";
        file_put_contents($fileName, "\xEF\xBB\xBF" . $data, FILE_APPEND); //dodaje kodowanie UTF-8
    }

    public function getInformationAboutUser($tableName, $primaryKey){

        if($tableName == "user_detail" || $tableName == "user"){
            $query = "SELECT ud.user_name, u.user_email, d.dict_value
					FROM user_details as ud
					JOIN users as u ON u.user_id = ud.user_id 
					JOIN dictionary as d 
					ON d.dict_order = u.user_origin and d.dict_type ='ORIGIN'
					WHERE u.user_id =".$primaryKey."
					";
        }else{

            $array = array(
                "announcement"=>"ann",
                "abuse"=>"abuse",
                "message"=>"msg",
                "payment"=>"pmt"
            );

            $query = "SELECT ud.user_name, u.user_email, d.dict_value
					FROM user_details as ud
					JOIN users as u ON u.user_id = ud.user_id 
					JOIN dictionary as d 
					ON d.dict_order = u.user_origin and d.dict_type ='ORIGIN'
					JOIN ".$tableName."s as tab2 ON ud.user_id = tab2.".$array[$tableName]."_user_id
					WHERE tab2.".$array[$tableName]."_id =".$primaryKey."
					";

        }

        $result = $this->_db->fetchRow($query);
        return $result;
    }


}