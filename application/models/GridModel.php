<?php

abstract class Application_Model_GridModel
{
    public function __construct(){
    }

    public abstract function getOne($id);
    public abstract function getAll();
    public abstract function delete($id);

}