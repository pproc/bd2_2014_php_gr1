		
		
		function updateRow(id)
		{
			parentTr = $("#" + id);
			
			inputArray = $(parentTr).find(".editableInput");
			spanArray = $(parentTr).find(".editableValue");
		
			$(spanArray).each(function(index)
			{
				value = $(inputArray[index]).val();
				$(this).html(value);
			});
		}
	
		function disableEditMode(id)
		{
			oldParentTr = $("#" + id);
				
			$(oldParentTr).find(".editableValue").each(function()
			{
				$(this).show();
			});
			
			$(oldParentTr).find(".editableInput").each(function()
			{
				$(this).hide();
			});

			$(oldParentTr).find(".editButton").text("edytuj");	
		}
		
		function enableEditMode(id)
		{
			parentTr = $("#" + id);
			
			$(parentTr).find(".editableValue").each(function()
			{
				$(this).hide();
			});
				
			$(parentTr).find(".editableInput").each(function()
			{
				$(this).show();
			});				
				
			$(parentTr).find(".editButton").text("zapisz");
		}
	
		$(document).ready( function()
		{	
			var editingId  = "";
			
			$( ".editButton" ).live('click', function()
			{
				if($(this).text() == "edytuj")
				{
					if(editingId)
					{
						disableEditMode(editingId);
					}
			
					editingId = $(this).closest("tr").attr("id");
				
					enableEditMode(editingId);
				}
				else
				{
					parentTr = $("#" + editingId);
				
					var args = {id : editingId};

					$(parentTr).find("input").each(function()
					{
						key = $(this).attr("name");
						args[key] = $(this).val();
					});
					
					$.ajax({
						type:		"POST",
						url:		ajaxURL,
						data:		args,
						async:		false,
						success: 	function(response)
									{
										switch(response)
										{
											case "OK":
											disableEditMode(editingId);
											updateRow(editingId);
											break;
											case "INVALID":
											alert("Nieprawid�owe dane");
											break;
											case "ERROR":
											disableEditMode(editingId);
											alert("Wyst�pi� b��d");
											break;
											case "BUSY":
											alert("Nazwa jest ju� zaj�ta");
											break;
											default:
											disableEditMode(editingId);
											break;	
										}	
									}
						});	
				}
			});
			
		});	