﻿/*
 * obsługa listy z sugestiami
 */
 
			$("#recipient").keydown(function(e)
			{
				var code = e.keyCode;

				if(code == 13)
				{
					return false;
				}
			});

			$("#recipient").keydown(function(e)
			{
				var code = e.keyCode;
				
				if(code == 40) 
				{ 
				   moveDown();
				}
				if(code == 38)
				{
					moveUp();
				}
			});

			function moveDown()
			{
				setItemHighlight(index, false);
				setItemHighlight(++index, true);
			}

			function moveUp()
			{
				setItemHighlight(index, false);
				setItemHighlight(--index, true);
			}
			
			function setItemHighlight(pos, value)
			{
				var items = $("#suggestionBox").find(".suggestionItem");

				if(pos > items.length - 1)
				{
					index = pos = 0;
				}
				if(pos < 0)
				{
					index = pos = items.length - 1;
				}
				
				if(value == true)
				{
					$(items[pos]).addClass("highlight");
				}
				if(value == false)
					$(items[pos]).removeClass("highlight"); 
			}	
			
			$("#recipient").keyup(function(e)
			{
				var code = e.keyCode;
				
				if(code == 13)
				{
					enterItem();
				}
				if(code != 38 && code != 40 && code != 13)
				{
					autoFill();
				}
			});

			function enterItem()
			{
				if(listSize > 0)
				{
					var items = $("#suggestionBox").find(".suggestionItem");	
					var selectedItem = $(items[index]);
				
					var id = $(selectedItem).find("input").val();
					var description = $(selectedItem).find("span").html();
					
					if(!insertRecipient(id, description))
						var h = 0;
				}

				resetSuggestionBox();
			}

			function resetSuggestionBox()
			{
				$("#suggestionBox").hide();
				$("#recipient").val("");
				index = 0;
			}

			function autoFill()
			{
				var value = $("#recipient").val();
				
				$.ajax
				({
					type: "POST",
					data: {
							value: value
						  },
					url: "/users/ajaxsearch",
					success: function(data)
					{
						$("#suggestionBox").html(data);
						bindItems();
					}
				});
			}

			function bindItems()
			{
				var items = $("#suggestionBox").find(".suggestionItem");
				
				$(items).each(function()
				{
					$(this).mouseover(function()
					{
						setItemHighlight(index, false);
						index = $(this).attr("id");
						setItemHighlight(index, true);
					});
					
					$(this).click(function()
					{
						enterItem();
					});
				});
								
				listSize = items.length;			
				if(listSize == 0)
				{
					$("#suggestionBox").hide();
				}
				else
				{
					setItemHighlight(0, true);
					$("#suggestionBox").show();
				}
			}



		function insertRecipient(id, description)
		{
			if($("#item_" + id).length != 0)
			{
				return 0;
			}
			
			if(currentId == 0)
			{
				var inputs = $("#recipientList").find("input");
				currentId = inputs.length / 2;
			}
			
			var li = "<li id='item_" + id + "'>";
			li += "<input type='hidden' name='id_" + currentId + "' value='" + id + "'/>";
			li += "<p style='float:left'>" + description + "</p>";
			li += "<input type='button' value='usuń' class='delete' onclick='deleteRecipient(" + id + ")'/>";
			li += "</li>";
			
			$("#recipientList").append(li);
			
			currentId++;
			
			$("#recipientCount").val(currentId);
		}
		
/*
 * popup z adresami
 */
 
		dragMode = false;
		left = 0;
		top = 0;

		function showAddressBox()
		{
			$("#addressBox").show();	
		}

		function closeAddressBox()
		{
			$("#addressBox").hide();
		}

		function moveButtonDown(e)
		{
			dragMode = true;

			var offset = $("#addressBox").offset();

			left = offset.left;
			top = offset.top;
			
			left = Math.floor(e.pageX - left);
			top = Math.floor(e.pageY - top);	
		}

		function moveButtonUp()
		{
			dragMode = false;
		}

		$(document).mousemove(function(e)
		{
			if(dragMode == true)
			{
				x = e.pageX - left;
				y = e.pageY - top;

				$("#addressBox").css("left", x);
				$("#addressBox").css("top", y);
			}
		})

		function selectAll()
		{
			 var checkboxes = $("#addressBox :checkbox");
			 
			 $(checkboxes).each(function()
			 {
				$(this).attr("checked", "checked");
			 });
		}

		function addRecipients()
		{
			 var checkboxes = $("#addressBox :checkbox");
			 
			 $(checkboxes).each(function()
			 {
				if($(this).attr("checked") == "checked")
				{
					var id = $(this).attr("rel");
					
					var address = $("#address_" + id);
					var description = $(address).find("span").html();;
					
					insertRecipient(id, description);
				}
			 });
			 
			 closeAddressBox();
		}


		function deleteRecipient(id)
		{
			var items = $("#recipientList").find("li");
			
			$("#item_" + id).remove();
		}