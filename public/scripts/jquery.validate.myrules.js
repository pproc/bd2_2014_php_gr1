jQuery.validator.addMethod("username", checkUsername);

function checkUsername(value, element)
{
	if(value.length < 2 || value.length > 60)
		return false;
		
	return /^[ _\-\u0041-\u005A\u0061-\u007A\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u00FF\u0100-\u0148\u014A-\u017F\u0180-\u024F\u0250-\u02AF\u0370-\u03FF\u0400-\u04FF\u0500-\u052F\u0530-\u058F\u0590-\u05FF\u0600-\u06FF\u0700-\u074F0-9\.]{2,60}$/.test(value);
}
