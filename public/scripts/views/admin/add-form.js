$(document).ready(function () {

    $('#mainAdminForm').validate({
        rules:{
            login:{
                required:true,
                email:true,
                remote:{
                    type:"POST",
                    url:"/admin/check-if-admin-exists",
                    //dataType : 'json',
                    data:{
                        adminEmail:function () {
                            return $("#login").val();
                        }
                    }
                }
            },
            password:{
                required:true,
                minlength:3
            },
            repeatPassword:{
                required:true,
                equalTo:"#password"
            },
            is_superadmin:{
                required:true
            }
        },

        onkeyup:false,

        messages:{
            login:{
                required:"To pole jest wymagane",
                email:"Nieprawidłowy format adresu e-mail",
                remote:"Dany email istnieje już w bazie danych"
            },
            password:{
                required:"To pole jest wymagane",
                minlength:"Minimala liczba znaków hasła to 3"
            },
            repeatPassword:{
                required:"To pole jest wymagane",
                equalTo:"Wprowadzone hasła nie są identyczne"
            },
            is_superadmin:{
                required:"To pole jest wymagane"
            }
        }
    });


});