$(document).ready(function () {

    $(".delButton").click(function () {
        var adminID = $(this).attr("id").split("-")[1];

        $(this).parent().parent().remove();

        deleteByAjax(adminID)
    });

});

function deleteByAjax(adminID) {
    $.ajax({
        type:"GET",
        url:"/admin/delete",
        //dataType : 'json',
        data:{
            id:adminID
        },
        success:function (data) {


        },
        error:function (jqXHR, exception) {
            alert("AJAX call an epic failure");
        }
    });

}