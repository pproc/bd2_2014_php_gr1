$(document).ready(function(){
    $('#editAdminForm').validate({
        rules:
        {
            previousPassword:{
                required: true,
                minlength: 3
            },
            password: {
                required: true,
                minlength: 3
            },
            repeatPassword:{
                required:true,
                equalTo:"#password"
            }
        },

        onkeyup: false,

        messages:
        {
            previousPassword:{
                required: "To pole jest wymagane",
                minlength: "Minimala liczba znaków hasła to: 3"
            },
            password:{
                required: "To pole jest wymagane",
                minlength: "Minimala liczba znaków hasła to: 3"
            },
            repeatPassword:{
                required:"To pole jest wymagane",
                equalTo:"Wprowadzone hasła nie są identyczne"
            }
        }

    });

});

