$(document).ready(function () {
    $('#editAdminForm').validate({
        rules:{
            login:{
                required:true,
                email:true,
                customvalidation:true

            }
        },

        onkeyup:false,

        messages:{
            login:{
                required:"To pole jest wymagane",
                email:"Login musi być adresem e-mail",
                customvalidation:"Dany mail jest już zajęty, podaj inny"
            }
        }

    });


    function checkIfAdminAlreadyExists(adminLogin) {
        var result = true;
        $('#adminList option').each(function () {
            if ($(this).val() == adminLogin && $(this).val() != $("#previous_login").val()) {
                result = false;
            }
        });
        return result;
    }


    $.validator.addMethod("customvalidation",
        function (value) {
            return checkIfAdminAlreadyExists(value);
        }
    );

});