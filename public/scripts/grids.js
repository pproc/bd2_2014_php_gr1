function Confirm(url)
{
    var confirmed = confirm("Czy jesteś pewien?");

    if (confirmed){ window.location = url; }
}

function ClearForm(formId)
{
    $("#" + formId).find("input").each(function()
    {
        if($(this).attr("type") == "text") $(this).val("");

        $(this).removeAttr("checked");
    });

    $("#" + formId).find("select").each(function()
    {
        if($(this).attr("name") != "perPage")
        {
            $(this).val("");
        }
    });

    $("#" + formId).submit();
}

function AllCheckboxToggle(object)
{
    if($(object).attr("checked"))
    {
        $(document).find("input.itemCheckbox").each(function()
        {
            $(this).attr("checked", "checked");
        });
    }
    else
    {
        $(document).find("input.itemCheckbox").each(function()
        {
            $(this).removeAttr("checked");
        });
    }
}

function SubmitAction(object)
{
    if($(object).val() == "0")
    {
        return;
    }

    var itemsSelected = $(document).find("input.itemCheckbox:checked").length;

    if(itemsSelected <= 0)
    {
        $(object).val("0");
        alert("Nie wybrano żadnych elementów");
        return;
    }

    var actionName = $("#" + $(object).attr("id") + " option:selected").text();

    var confirmed = confirm("Czy jesteś pewien, że chcesz wykonać następującą akcję: '" + actionName + "' na " + itemsSelected + " items?");

    if (confirmed){ $("#formAction").submit(); }
}

function SubmitAction2(object, actionFormName)
{

    if($(object).val() == "0"){
        return;
    }

    var itemsSelected = $(document).find("input.itemCheckbox:checked").length;

    if(itemsSelected <= 0){
        //pobieram nazwę klasy
        var classesName = object.attr("class").split(" ");
        var className = classesName[classesName.length-1];
        $("."+className).val("0");
        alert("Nie wybrano żadnych elementów");
        return;
    }

    var actionName = $("#" + $(object).attr("class") + " option:selected").text();

    if(itemsSelected==1){
        var nazwaIlosciWierszy = " wierszu?"
    } else{
        var nazwaIlosciWierszy = " wierszach?"
    }

    if($(object).val()=="setAccount"){

        if(itemsSelected>1){
            alert("Akcję zmiany konta możesz wykonać jedynie na pojedynczym użytkowniku");
            return;
        }

        var gender = $(document).find("input.itemCheckbox:checked").parent().parent().find(".genderCell").html();

        if(gender.trim() == "kobieta"){
            $("#linkToRevealModal2").click();
            $("#selectWomanTypeAccount").attr("name","selectTypeAccount");
        }else{
            $("#linkToRevealModal").click();
            $("#selectManTypeAccount").attr("name","selectTypeAccount");
        }

        $(".selectConfirm").change(function(){

            $("#"+actionFormName).submit();
        });

    }else if($(object).val()=="generateRaport") {
        $("#"+actionFormName).submit();
    }else{

        var confirmed = confirm("Czy jesteś pewien, że chcesz wykonać następującą akcję: '" + actionName + "' na " + itemsSelected + nazwaIlosciWierszy);

        if (confirmed){
            $("#"+actionFormName).submit();
        }
        //alert("Operacja została wykonana poprawnie.");
    }
    //$("#actionSelect").val("");
}

function RemoveItem(url)
{
    var confirmed = confirm("Jesteś pewien, że chcesz usunąć ten element?");

    if (confirmed){ window.location.href = url; }
}
