(function($)
{
	$.fn.extend(
	{
        passwordStrengthValidator: function(options)
		{
			/*
			 * default values
			 */
            var defaults =
			{
				colors: Array( "darkred", "red", "orange", "darkred", "green", "darkgreen" ),
				descriptions: Array(
									"Unacceptable",
									"Very weak",
									"Weak",
									"Average",
									"Strong",
									"Very strong"
								),
				results: this,
				classPrefix: "strengthBar-",
				checkEmpty: false
			};
			
			var options = $.extend(defaults, options);
			
			/*
			 * append additional elements
			 */
			if(options.results == this)
			{
				$('<p id="strengthBar" style="width: 250px; ">&nbsp;</p>').insertAfter(options.results);
				$('<p id="passwordStrengthDesc" style="width: 250px;"></p>').insertAfter(options.results);
			}
			else
			{
				$(options.results).append('<p id="strengthBar" style="width: 250px;">&nbsp;</p>');
				$(options.results).append('<p id="passwordStrengthDesc" style="width: 250px;"></p>');
			}
			
			/*
			 * functions
			 */
			$(this).keyup(function()
			{
				
				if(options.checkEmpty == false && $(this).val().length == 0)
				{
					$(" #strengthBar ").removeAttr("class");
					$(" #passwordStrengthDesc ").html('');
					return false;
				}
				
				var score = 0;
				var password = $(this).val();
				var minLength = 6;
				
				if (password.length >= minLength)
				{
					score++;
						
					if ((password.match(/[a-z]/)) && (password.match(/[A-Z]/))) 
						score++;
						
					if (password.match(/\d+/)) 
						score++;
						
					if (password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) 
						score++;

					if (password.length > 12)
						score++;
				}

				$(" #strengthBar ").attr("class",options.classPrefix+score);
				$(" #strengthBar ").show();
				$(" #passwordStrengthDesc ").css("color",options.colors[score]);
				$(" #passwordStrengthDesc ").html(options.descriptions[score]);
			});
			
        }
    });
})
(jQuery);

