function OnlineStamp()
{
	$.ajax(
	{
		url: "/user/update-online-stamp",
		type: "POST",
		dataType: "json"
	});
};

$(document).ready(function()
{
	OnlineStamp();

	setInterval(OnlineStamp, 60000);
});