/*
 * Translated default messages for the jQuery validation plugin.
 * Language: PL
 */
jQuery.extend(jQuery.validator.messages, {
	shortText: "To pole powinno mieć maksymalnie 50 znaków",
	mediumText: "To pole powinno mieć maksymalnie 255 znaków",
	longText: "To pole powinno mieć maksymalnie 12000 znaków",
	login: "Proszę podać prawidłowy login.",
	password: "Hasło powinno mieć długość 6-20 znaków.",
	firstname: "Imię nie powinno zawierać znaków specjalnych ani cyfr.",
	surname: "Nazwisko nie powinno zawierać znaków specjalnych ani cyfr.",
	city: "Nieprawidłowe miasto.",
	address: "Nieprawidłowy adres.",
	recipientCount: "Prosze wybrać przynajmniej jednego odbiorcę.",
	phone: "Nieprawidłowy nr telefonu.",
	hour: "Proszę podać prawidłową godzinę.",
	required: "To pole jest wymagane.",
	remote: "Proszę o wypełnienie tego pola.",
	email: "Proszę o podanie prawidłowego adresu email.",
	url: "Proszę o podanie prawidłowego URL.",
	date: "Proszę o podanie prawidłowej daty.",
	dateISO: "Proszę o podanie prawidłowej daty (ISO).",
	number: "Proszę o podanie prawidłowej liczby.",
	digits: "Proszę o podanie samych cyfr.",
	creditcard: "Proszę o podanie prawidłowej karty kredytowej.",
	equalTo: "Proszę o podanie tej samej wartości ponownie.",
	accept: "Proszę o wybraniu pliku z prawidłowym rozszerzeniem.",
	maxlength: jQuery.validator.format("Proszę o podanie nie więcej niż {0} znaków."),
	minlength: jQuery.validator.format("Proszę o podanie przynajmniej {0} znaków."),
	rangelength: jQuery.validator.format("Proszę o podanie wartości o długości od {0} do {1} znaków."),
	range: jQuery.validator.format("Proszę o podanie wartości z przedziału od {0} do {1}."),
	max: jQuery.validator.format("Proszę o podanie wartości mniejszej bądź równej {0}."),
	min: jQuery.validator.format("Proszę o podanie wartości większej bądź równej {0}.")
});