	var index = 0;
	var listSize = 0;
	var currentId = 0;

	$(document).click(function() 
	{ 
		resetSuggestionBox();
	}); 

	$(document).ready(function()
	{
	
			$("#recipient").keydown(function(e)
			{
				var code = e.keyCode;

				if(code == 13)
				{
					return false;
				}
			});	
			
			$("#recipient").keydown(function(e)
			{
				var code = e.keyCode;
				
				if(code == 40) 
				{ 
				   moveDown();
				}
				if(code == 38)
				{
					moveUp();
				}
			});		

			$("#recipient").keyup(function(e)
			{
				var code = e.keyCode;
				
				if(code == 13)
				{
					enterItem();
				}
				if(code != 38 && code != 40 && code != 13)
				{
					autoFill();
				}
			});			
	
	});





			function moveDown()
			{
				setItemHighlight(index, false);
				setItemHighlight(++index, true);
			}

			function moveUp()
			{
				setItemHighlight(index, false);
				setItemHighlight(--index, true);
			}
			
			function setItemHighlight(pos, value)
			{
				var items = $("#suggestionBox").find(".suggestionItem");

				if(pos > items.length - 1)
				{
					index = pos = 0;
				}
				if(pos < 0)
				{
					index = pos = items.length - 1;
				}
				
				if(value == true)
				{
					$(items[pos]).addClass("highlight");
				}
				if(value == false)
					$(items[pos]).removeClass("highlight"); 
			}	
			


			function enterItem()
			{
				if(listSize > 0)
				{
					var items = $("#suggestionBox").find(".suggestionItem");	
					var selectedItem = $(items[index]);
				
					var id = $(selectedItem).find("input").val();
					var description = $(selectedItem).find("span").html();
					
					if(!insertRecipient(id, description))
						var h = 0;
				}

				resetSuggestionBox();
			}

			function resetSuggestionBox()
			{
				$("#suggestionBox").hide();
				//$("#recipient").val("");
				index = 0;
			}

			function autoFill()
			{
				$("#recipientId").val("");
				var value = $("#recipient").val();
				
				$.ajax
				({
					type: "POST",
					data: {
							value: value
						  },
					url: "/users/ajaxsearch",
					success: function(data)
					{
						$("#suggestionBox").html(data);
						bindItems();
					}
				});
			}

			function bindItems()
			{
				var items = $("#suggestionBox").find(".suggestionItem");
				
				$(items).each(function()
				{
					$(this).mouseover(function()
					{
						setItemHighlight(index, false);
						index = $(this).attr("id");
						setItemHighlight(index, true);
					});
					
					$(this).click(function()
					{
						enterItem();
					});
				});
								
				listSize = items.length;			
				if(listSize == 0)
				{
					$("#suggestionBox").hide();
				}
				else
				{
					setItemHighlight(0, true);
					$("#suggestionBox").show();
				}
			}

			function insertRecipient(id, description)
			{
				$("#recipient").val(description);
				$("#recipientId").val(id);
				$("#recipientId").valid(); 
			}