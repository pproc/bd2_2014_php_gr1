<?php

if (get_magic_quotes_gpc()) 
{
	$process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);

	while (list($key, $val) = each($process)) 
	{
		foreach ($val as $k => $v)
		{
			unset($process[$key][$k]);
			
			if (is_array($v)) 
			{
				$process[$key][stripslashes($k)] = $v;
				
				$process[] = &$process[$key][stripslashes($k)];
			}
			else
			{
				$process[$key][stripslashes($k)] = stripslashes($v);
			}
		}
	}
	
	unset($process);
}





error_reporting(E_ALL);


defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

defined('APPLICATION_ENV')  || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

set_include_path(implode(PATH_SEPARATOR, Array(realpath(APPLICATION_PATH . '/../library'), get_include_path(),)));




require_once 'Zend/Application.php';


$application = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini');


try
{
	$application->bootstrap();
	
	$bootstrap = $application->getBootstrap();
	
	$front = $bootstrap->frontController;
	
	$application->run();
}
catch(Exception $ex)
{
	print_r($ex->getMessage());
	
	exit;
}

