/*global Ext:false */
/*

This file is part of Ext JS 4

Copyright (c) 2011 Sencha Inc

Contact:  http://www.sencha.com/contact

GNU General Public License Usage
This file may be used under the terms of the GNU General Public License version 3.0 as published by the Free Software Foundation and appearing in the file LICENSE included in the packaging of this file.  Please review the following information to ensure the GNU General Public License version 3.0 requirements will be met: http://www.gnu.org/copyleft/gpl.html.

If you are unsure which license is appropriate for your use, please contact the sales department at http://www.sencha.com/contact.

*/
Ext.require([
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.tree.*'
]);

Ext.onReady(function() {
    //we want to setup a model and store instead of using dataUrl
    Ext.define('Task', {
        extend: 'Ext.data.Model',
        fields: [
			{name: 'id',     type: 'string'},
            {name: 'topic',     type: 'string'},
			{name: 'receiver',     type: 'string'},
            {name: 'date',     type: 'string'},
            {name: 'priority', type: 'string'},
			{name: 'name', type: 'string'},
			{name: 'email', type: 'string'},
			{name: 'sms', type: 'string'},
			{name: 'phone', type: 'string'},
			{name: 'content', type: 'string'},
			{name: 'rec_id', type: 'string'},
			{name: 'prio_val', type: 'string'}
        ]
    });

    var store = Ext.create('Ext.data.TreeStore', {
        model: 'Task',
        proxy: {
            type: 'ajax',
            //the store will get the content from the .json file
            url: '/public/plugins/tree_main_panel/treegrid.php'
        },
        folderSort: true
    });

    //Ext.ux.tree.TreeGrid is no longer a Ux. You can simply use a tree.TreePanel
    var tree = Ext.create('Ext.tree.Panel', {
        title: 'Twoje zadania:',
        width: 700,
        height: 300,
        renderTo: 'renderTreeHere',
        collapsible: true,
        useArrows: true,
        rootVisible: false,
        store: store,
        multiSelect: true,
        singleExpand: true,
        //the 'columns' property is now 'headers'
        columns: [{
            text: 'id',
            flex: 1,
            sortable: true,
            dataIndex: 'id',
			hidden:true
        },{
            xtype: 'treecolumn', //this is so we know which column will show the tree
            text: 'Temat',
            flex: 4,
            sortable: true,
            dataIndex: 'topic'
        },{
            text: 'Odbiorca',
            flex: 3,
            dataIndex: 'name',
            sortable: true,
			align: 'center'
        },{
            text: 'E-mail odbiorcy',
            flex: 2,
			hidden:true,
            sortable: true,
            dataIndex: 'receiver'
        },{
            text: 'Data',
            flex: 2,
            sortable: true,
            dataIndex: 'date',
            align: 'center',
        },{
            text: 'Priorytet',
            flex: 2,
            dataIndex: 'priority',
            sortable: true,
			align: 'center'
        },{
            text: 'email',
            flex: 1,
            sortable: true,
            dataIndex: 'email',
			hidden:true
        },{
            text: 'sms',
            flex: 1,
            sortable: true,
            dataIndex: 'sms',
			hidden:true
        },{
            text: 'phone',
            flex: 1,
            sortable: true,
            dataIndex: 'phone',
			hidden:true
        },{
            text: 'content',
            flex: 1,
            sortable: true,
            dataIndex: 'content',
			hidden:true
        },{
            text: 'rec_id',
            flex: 1,
            sortable: true,
            dataIndex: 'rec_id',
			hidden:true
        },{
            text: 'prio_val',
            flex: 1,
            sortable: true,
            dataIndex: 'prio_val',
			hidden:true
        }],
		viewConfig: { 
        stripeRows: false, 
        getRowClass: function(record) { 
			return 'context-menu-leaf'; 
        }}
    });
});

