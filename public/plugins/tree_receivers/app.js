/*global Ext:false */
/*

This file is part of Ext JS 4

Copyright (c) 2011 Sencha Inc

Contact:  http://www.sencha.com/contact

GNU General Public License Usage
This file may be used under the terms of the GNU General Public License version 3.0 as published by the Free Software Foundation and appearing in the file LICENSE included in the packaging of this file.  Please review the following information to ensure the GNU General Public License version 3.0 requirements will be met: http://www.gnu.org/copyleft/gpl.html.

If you are unsure which license is appropriate for your use, please contact the sales department at http://www.sencha.com/contact.

*/
Ext.require([
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.tree.*'
]);

Ext.onReady(function() {
    //we want to setup a model and store instead of using dataUrl
    Ext.define('Task', {
        extend: 'Ext.data.Model',
        fields: [
			{name: 'id',     type: 'string'},
			{name: 'name',     type: 'string'},
            {name: 'email',     type: 'string'}

        ]
    });

    var store = Ext.create('Ext.data.TreeStore', {
        model: 'Task',
        proxy: {
            type: 'ajax',
            //the store will get the content from the .json file
            url: '/public/plugins/tree_receivers/treegrid.php'
        },
        folderSort: true
    });

    //Ext.ux.tree.TreeGrid is no longer a Ux. You can simply use a tree.TreePanel
    var tree = Ext.create('Ext.tree.Panel', {
        title: 'Twoi odbiorcy:',
        width: 700,
        height: 300,
        renderTo: 'renderTreeHere',
        collapsible: true,
        useArrows: true,
        rootVisible: false,
        store: store,
        multiSelect: true,
        singleExpand: true,
        //the 'columns' property is now 'headers'
        columns: [{
            text: 'id',
            flex: 1,
            sortable: true,
            dataIndex: 'id',
			hidden:true
        },{
			xtype: 'treecolumn', //this is so we know which column will show the tree
            text: 'Nazwa',
            flex: 1,
            sortable: true,
            dataIndex: 'name'
        },{
            text: 'E-mail',
            flex: 1,
            sortable: true,
            dataIndex: 'email'
        }],
		viewConfig: { 
        stripeRows: false, 
        getRowClass: function(record) { 
			return 'context-menu-leaf'; 
        }}
    });
});

