<?php

defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
defined('APPLICATION_ENV')  || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

set_include_path(implode(PATH_SEPARATOR, Array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));


require_once 'Zend/Application.php';
// Dołączenie pakietu NuSOAP

require_once("../library/soap/lib/nusoap.php");

// Utworzenie usługi sieciowej (serwera SOAP)
$ns = "http://memomail.com.pl";

$serwer = new soap_server();

$serwer->configureWSDL('Operacja płatności', $ns);

$serwer->wsdl->schemaTargetNamespace = $ns;

$serwer->register('addPayment',array(
        'client_id' => 'xsd:integer',
        'product_name' => 'xsd:string'

    ), //lista parametrow
    array('return' => 'xsd:string'), //co zwraca
    $ns,false,false,false,'Operacja zakupu.');


function addPayment($client_id, $product_name) {

    $application = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini');
    $application->bootstrap();
    $db = Zend_Db_Table::getDefaultAdapter();

    $query = "INSERT INTO payments(pmt_user_id, pmt_product_name)
            VALUES($client_id, '$product_name')";

    $result =  $db->query($query);

    return new soapval('return', 'xsd:string',$query);
}


$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';

$serwer->service($HTTP_RAW_POST_DATA);

?>