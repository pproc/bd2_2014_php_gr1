<?php

defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
defined('APPLICATION_ENV')  || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

set_include_path(implode(PATH_SEPARATOR, Array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));


require_once 'Zend/Application.php';
// Dołączenie pakietu NuSOAP

require_once("../library/soap/lib/nusoap.php");

// Utworzenie usługi sieciowej (serwera SOAP)
$ns = "http://erp.esuite.pl";

$serwer = new soap_server();

$serwer->configureWSDL('Operacje na fakturach w systemie ERP', $ns);

$serwer->wsdl->schemaTargetNamespace = $ns;

// add complex type
$serwer->wsdl->addComplexType('Produkt','complexType','struct','all','',
    array(
        'id' => array('name'=>'id', 'type'=>'xsd:integer'),
        'type' => array('name'=>'type', 'type'=>'xsd:string'),
        'quantity' => array('name'=>'quantity', 'type'=>'xsd:integer'),
        'license' => array('name'=>'license', 'type'=>'xsd:string')
    ));

$serwer->wsdl->addComplexType(
// name
    'ProduktArray',
    // typeClass (complexType|simpleType|attribute)
    'complexType',
    // phpType: currently supported are array and struct (php assoc array)
    'array',
    // compositor (all|sequence|choice)
    '',
    // restrictionBase namespace:name (http://schemas.xmlsoap.org/soap/encoding/:Array)
    'SOAP-ENC:Array',
    // elements = array ( name = array(name=>'',type=>'') )
    array(),
    // attrs
    array(
        array(
            'ref' => 'SOAP-ENC:arrayType',
            'wsdl:arrayType' => 'tns:Produkt[]'
        )
    ),
    // arrayType: namespace:name (http://www.w3.org/2001/XMLSchema:string)
    'tns:Produkt'
);

$serwer->register('addFacture',array(
        'client_id' => 'xsd:integer',
        'buyer_id' => 'xsd:integer',
        'products' => 'tns:ProduktArray',
        'is_paid' => 'xsd:boolean'

    ), //lista parametrow
    array('return' => 'xsd:int'), //co zwraca
    $ns,false,false,false,'Tworzy nowa fakture.');


function addFacture($client_id,$buyer_id, $products, $is_paid) {

    $application = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini');
    $application->bootstrap();
    $db = Zend_Db_Table::getDefaultAdapter();


    //klient
    $query = "
		SELECT * FROM kontrahenci WHERE id = $client_id
	";
    $client =  $db->fetchRow($query);

    //pobieram wartosci z klienta
    $nip = $client["nip"];
    $client_name = $client["name"];

    $address = $client["home_num"]."/".$client["local_num"]." ".$client["postal_code"]." "
        .$client["city"]." ".$client["country"];

    $delivery_address = $address;

    $city = $client["city"];

    //sprzedawca
    $query = "
		SELECT * FROM kontrahenci WHERE id = $buyer_id
	";

    $buyer =  $db->fetchRow($query);
    $buyer_name = $buyer["name"];

    //inne
    $create_date = date("Y-m-d");
    $sell_date  = date("Y-m-d");

    $limit_date = date('Y-m-d', strtotime($create_date. ' + 14 days'));

    $fac_number = "";
    $customer = "";

    $is_paid = (string) $is_paid;

    if($is_paid ==0){
        $is_paid = 'f';
    }else{
        $is_paid = 't';
    }


    $query = "
		INSERT INTO factures(
            client, address, nip,  fac_number, delivery_address, create_date, 
            sell_date, limit_date, city, customer, seller, paid)
		VALUES ('$client_name', '$address', '$nip', '$fac_number', '$delivery_address', '$create_date', 
				'$sell_date', '$limit_date', '$city', '$customer', '$buyer_name', '$is_paid');
	";

    // print_r($query);
    // exit;

    $result =  $db->query($query);

    $con_id_q = $db->query("select id from factures order by id desc limit 1");
    $con_id = $con_id_q->fetchAll();
    $con_id = $con_id[0]['id'];


    foreach($products as $product){
        $id = $product->id;
        $type = $product->type;
        $quantity = $product->quantity;
        $license = $product->license;

        $query = "SELECT * FROM products
			WHERE product_id = $id --and license = '$license'
		";
        $result = $db->fetchRow($query);

        $id = $result["product_id"];
        $price = $result["price"];
        $gross_price = $result["gross_price"];

        if($type=="TEXT" || $type=="PHOTO" || $type=="RECORD" || $type=="VIDEO"){

            $table_name = "storage_".strtolower($type)."s";
            $type_id = strtolower($type)."_id";
            $query = "SELECT * FROM $table_name WHERE $type_id=$id";

            $result = $db->fetchRow($query);
            $product = $result["title"];
        }else{
            print_r("Podany produkt nie jest przydzielony do ¿adnej z kategorii.");
            exit;
        }

        $result = $db->query($query);
        $query = "
		INSERT INTO products_in_factures(
            facture_id, id, product,  amount, net_price, 
					vat_value) 
			VALUES ($con_id, $id, '$product', $quantity, '$price', 
					'$gross_price')
		";

        $result = $db->query($query);
    }

    return new soapval('return', 'xsd:int',$con_id);
}

//addFacture($_GET["a"], $_GET["b"]);

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';

$serwer->service($HTTP_RAW_POST_DATA);

?>