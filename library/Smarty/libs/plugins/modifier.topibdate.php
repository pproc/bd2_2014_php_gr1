<?php

function smarty_modifier_topibdate($string)
{
	$tab = explode(" ", $string);
	
	$dateTab = explode("-", $tab[0]);
	
	$hourTab = explode(":", $tab[1]);
	
	return $dateTab["2"] . "/" . $dateTab["1"] . "/" . $dateTab["0"] . " " . $hourTab[0] . ":" . $hourTab[1];
} 

?>