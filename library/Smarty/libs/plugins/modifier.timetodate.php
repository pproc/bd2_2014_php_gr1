<?php

/**
 * Zamienia wartość czasu PGSQL na samą datę
 */

function smarty_modifier_timetodate($string)
{
	$tab = split(" ", $string);
	
	$result = $tab[0];
	
	if(!$result)
	{
		$result = "-----";
	}
	
    return $result;
} 

?>