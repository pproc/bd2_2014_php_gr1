<?php

function smarty_function_control($params, $template)
{
	if($params["type"] == "menu")
	{
		unset($params["type"]);
	
		$control = new Application_Controls_Menu(json_encode($params), null);
		
		return $control->render();
	}
	if($params["type"] == "loginForm")
	{
		unset($params["type"]);
	
		$control = new Application_Controls_LoginForm(json_encode($params), null);
		
		return $control->render();
	}	
	if($params["type"] == "placeholder")
	{
		$control = new Application_Controls_PlaceHolder($params);
		
		return $control->render();
	}
} 

?>