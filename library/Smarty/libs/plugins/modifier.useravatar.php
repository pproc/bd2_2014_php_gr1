<?php

function smarty_modifier_useravatar($value, $gender, $size)
{
	if($value)
	{
		$filePath = "/public/images/photos/$size/" . $value;
		
		if(file_exists($filePath))
		{
			return $filePath;
		}
		
		return $filePath;
	}
	else
	{
		if($gender == 0)
		{
			return "/public/images/photos/defaultFemalePhoto.jpg";
		}
		if($gender == 1)
		{
			return "/public/images/photos/defaultMalePhoto.jpg";
		}
	}
} 

?>