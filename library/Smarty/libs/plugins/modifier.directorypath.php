<?php

/**
 * Zwraca ścieżkę katalogu
 */

function smarty_modifier_directorypath($string, $level)
{ 

	$tab = explode("/", $string);

	if(count($tab) == $level + 1)
	{
	
		$result = Array();
	
		for($i = 0; $i < $level; $i++)
		{
			array_push($result, $tab[$i]);
		}
		
		return implode("/", $result);
	}
	else
	{
		return 0;
	}
	
	//return $string."xxx".$level;
    //return $upper_string;
} 

?>