<?php


function smarty_function_pagelink($params, $template)
{
	$exp = explode("/", $_SERVER['REQUEST_URI']);
	
	//print_r($exp);
	//exit;
	
	foreach($exp as $key=>$value)
	{
		if(!$value)
			unset($exp[$key]);
	}
	
	
	$deleteNext = false;
	foreach($exp as $key=>$value)
	{
	
		if($deleteNext == true)
		{
			unset($exp[$key]);
			$deleteNext = false;
		}
		if($value == "page" || $value == "searchId")
		{
			unset($exp[$key]);
			$deleteNext = true;
		}
		
	}
	
	if(isSet($params["page"]))
	{
		array_push($exp, "page");
		array_push($exp, $params["page"]);
	}
	
	if(isSet($params["searchId"]))
	{
		array_push($exp, "searchId");
		array_push($exp, $params["searchId"]);
	}
	
	return "/" . implode("/", $exp);
}

?>