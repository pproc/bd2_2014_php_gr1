<?php


function smarty_function_sortlink($params, $template)
{
	$icons = Array(
				"ASC" 	=> " >",//<img src='/public/images/arrow_up.gif'/>",
				"DESC"	=> "< "//<img src='/public/images/arrow_down.gif'/>"
				);

	$exp = explode("/", $_SERVER['REQUEST_URI']);
				
	//print_r($exp);
	//exit;
				
	$deleteNext = false;
	foreach($exp as $key=>$value)
	{
		if(!$value)
		{
			unset($exp[$key]);
		}
		
		if($deleteNext == true)
		{
			unset($exp[$key]);
			$deleteNext = false;
		}
		if($value == "order" || $value == "searchId")
		{
			unset($exp[$key]);
			$deleteNext = true;
		}
		
	}
	
	if(isSet($params["order"]) && isSet($params["column"]))
	{
		array_push($exp, "order");
		array_push($exp,  $params["column"]. "_" . $params["order"]);
	}
	
	//print_r($params);
	
	if(isSet($params["searchId"]))
	{
		array_push($exp, "searchId");
		array_push($exp, $params["searchId"]);
	}	
	
	$ref = implode("/", $exp);				
				
	$result = "<a href='/$ref'>" . $icons[$params["order"]] . "</a>";
	
	return $result;
}

?>