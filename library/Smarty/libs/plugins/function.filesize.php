<?php

/**
 * Podaje rozmiar pliku określony przez id pliku, id folderu i id grupy roboczej
 */
 
function smarty_function_filesize($params)
{ 
	$fileName = $params["fileName"];
	$directoryId = $params["directoryId"];
	$workspaceId = $params["workspaceId"];
	
	$filePath = $_SERVER["DOCUMENT_ROOT"] . "/public/uploads/workspaces/workspace_$workspaceId/directory_$directoryId/$fileName";
	
	if(file_exists($filePath))
	{
		return round(filesize($filePath) / 1024, 2) . " KB";
	}
	else
	{
		return "0 KB";
	}
} 

?>