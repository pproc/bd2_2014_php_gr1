<?php

function smarty_function_backgroundbydate($params, $template)
{
	//$pendingColor = $params['pending'];
	//$inactiveColor = $params['inactive'];

	$date = strtotime($params['date']);
	$currentDate = time();
	
	$dateDiff = $date - $currentDate;
	
	$fullDays = ceil($dateDiff/(60*60*24));
	
	$template->assign('tempDate', $fullDays);
	
	$config = Zend_Registry::get('config');
	$emphasis_before = $config->editable->emphasis_before;	
	
	$pendingColor = "#FF9999";
	$inactiveColor = "#DDDDDD";
	
	if($fullDays <= $emphasis_before && $fullDays >= 0)
		return "style='background-color: $pendingColor'";
	if($fullDays < 0)
		return "style='background-color: $inactiveColor'";
	
	//return $fullDays; 	
}

?>