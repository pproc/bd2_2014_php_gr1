<?php

class Application_AjaxController extends Application_MyController
{
	protected $_ajaxResponse;
	
	public function init()
	{
		parent::init();
		
		$this->_helper->layout()->setLayout('ajax');
		$this->_ajaxResponse = array('success' => 0, 'content' => '', 'errorMessage' => '', 'message' => '');
	}
	
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('default');
	}
	
	/*
	Ustawienie response ajax na success
	*/
	protected function _setAjaxSuccess()
	{
		$this->_ajaxResponse["success"] = '1';
	}
	
	/*
	Ustawianie odpowiedzi ajax
	*/
	protected function _setAjaxResponse($content)
	{
		$this->_ajaxResponse["content"] = $content;
	}
	
	/*
	Ustawianie bledu ajax
	*/
	protected function _setAjaxError($errorMessage)
	{
		$this->_ajaxResponse["errorMessage"] = $errorMessage;
	}
	
	/*
	Wyswietlanie odpowiedzi ajax
	*/
	protected function _ajaxResponse()
	{
		echo json_encode($this->_ajaxResponse);
		exit;
	}
	
	protected function _setAjaxMessage($message)
	{
		$this->_ajaxResponse["message"] = $message;
	}
	
}