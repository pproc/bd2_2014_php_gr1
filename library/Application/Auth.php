<?php

class Application_Auth
{
	static function getUserId()
	{
			if(Zend_Auth::getInstance()->hasIdentity())  
			{  
				$userData = Zend_Auth::getInstance()->getStorage()->read();
				$user_id  = $userData->user_id;
			}
			else $user_id = -1;
			
		return $user_id;
	}
	
	static function isUserLogged()
	{
		return Zend_Auth::getInstance()->hasIdentity();
	}
}