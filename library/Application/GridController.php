<?php

abstract class Application_GridController extends Application_MyController
{
    public function init()
    {
        parent::init();

        if (isset($_SESSION["MESSAGE"])) {
            $this->view->message = $_SESSION["MESSAGE"];
        }
        unset($_SESSION["MESSAGE"]);
    }

    public abstract function indexAction();
    public abstract function listAction();

    public abstract function addAction();
    public abstract function addFormAction();

    public abstract function editFormAction();

    public abstract function deleteAction();

}