<?php

class Application_Logger_DBUtil
{
	const UNDEFINED_RESULT = "<undefined>";
	
	public static function paramsFormat()
	{
		$args = func_get_args();
		$params = array();
		
		foreach($args as $arg)
			$params[] = $arg;
			
		return $params;
	}
	
	public static function userFullName($userId)
	{
		$query = "SELECT user_name,user_surname 
					FROM users WHERE user_id = '$userId'";
					
		$result = Zend_Db_Table::getDefaultAdapter()->query($query);
		
		if($row = $result->fetch())
			return $row['user_name'] . " " . $row['user_surname'];
			
		return self::UNDEFINED_RESULT;
	}
	
	public static function userTypeName($userTypeId)
	{
		$query = "SELECT user_type_name FROM user_types WHERE user_type_id = '$userTypeId'";
		
		$result = Zend_Db_Table::getDefaultAdapter()->query($query);
		
		if($row = $result->fetch())
			return $row['user_type_name'];
			
		return self::UNDEFINED_RESULT;
	}
	
	public static function workspaceName($workspaceId)
	{
		$query = "SELECT workspace_name FROM workspaces where workspace_id = '$workspaceId'";
		$result = Zend_Db_Table::getDefaultAdapter()->query($query);
		
		if($row = $result->fetch())
			return $row['workspace_name'];
			
		return self::UNDEFINED_RESULT;
	}
}