<?php

class Application_Logger_DB
{
	protected static $instance;
	protected static $db;
	protected static $messages;
	
	private function __construct()
	{
		self::$db = Zend_Db_Table::getDefaultAdapter();
	}
	
	public static function &getInstance()
	{
		if(!isset(self::$instance))
			self::$instance = new Application_Logger_DB();
			
		return self::$instance;
	}
	
	public static function log($objectId,$objectType,$actionType,$params="")
	{
		if(!isset(self::$instance))
			return false;
			
		$userId = Application_Auth::getUserId();
		
		if($userId == -1)
			$userId = 0;
			
		$code = strtoupper($objectType . "_" . $actionType);
		
		$message = self::_getLogMessage($code);

		if(is_array($params))
		{
			$i = 0;
			foreach($params as $param)
				$message = str_replace("$".(++$i),$param,$message);
			
			
		}
		elseif(trim($params != ""))
			$message = $params;
		
		$query = "INSERT	INTO	logs(
										user_id,
										object_id,
										log_object_name,
										log_action_name,
										log_comment,
										log_ip,
										log_host
										)
										
									VALUES(
										'$userId',
										'$objectId',
										'$objectType',
										'$actionType',
										'$message',
										'".Application_Util::getIp()."',
										'".Application_Util::getHost()."'
										)
				";
				
		return self::$db->query($query);
	}
	
	private function _getLogMessage($code)
	{
		if(!isset(self::$messages))
		{
			$messagesFile = $_SERVER["DOCUMENT_ROOT"] . "/application/configs/messages/logs.yml";
			self::$messages = new Zend_Config(Spyc::YAMLLoad($messagesFile));
		}
		
		if(isset(self::$messages->$code))
			return self::$messages->$code;

		return "";
	}
	
	/*
	public static function log($objectId,$objectType,$actionType)
	{
		if(!isset(self::$instance))
			return false;
			
		$userId = Application_Auth::getUserId();
		
		if($userId == -1)
			$userId = 0;
		
		if((int)$objectId < 1)
			return false;
		
		$query = "INSERT	INTO	logs(
										user_id,
										object_id,
										log_object_type,
										log_action_type
										)
										
									VALUES(
										'$userId',
										'$objectId',
										'$objectType',
										'$actionType'
										)
				";
				
		return self::$db->query($query);
	}
	*/
	
}