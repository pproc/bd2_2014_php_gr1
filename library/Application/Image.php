<?php

class Application_Image 
{ 
	public $imageResource;
	
	public $imageType;
	
	function load($fileName) 
	{
		if(exif_imagetype($fileName) == IMAGETYPE_JPEG)
		{
			$this->imageResource = imagecreatefromjpeg($fileName);
			
			$this->imageType = IMAGETYPE_JPEG;
		}
		if(exif_imagetype($fileName) == IMAGETYPE_GIF)
		{
			$this->imageResource = imagecreatefromgif($fileName);
			
			$this->imageType = IMAGETYPE_GIF;
		}
		if(exif_imagetype($fileName) == IMAGETYPE_PNG)
		{
			$this->imageResource = imagecreatefrompng($fileName);
			
			$this->imageType = IMAGETYPE_PNG;
		}
	}
	
	function save($fileName)
	{
		if($this->imageType == IMAGETYPE_JPEG) 
		{
			imagejpeg($this->imageResource, $fileName, 75);
		} 
		if($this->imageType == IMAGETYPE_GIF) 
		{
			imagegif($this->imageResource, $fileName);         
		}
		if($this->imageType == IMAGETYPE_PNG) 
		{
			imagepng($this->imageResource, $fileName);
		}   
	}
	
	function getWidth() 
	{
		return imagesx($this->imageResource);
	}
	
	function getHeight() 
	{
		return imagesy($this->imageResource);
	}
	
	function resizeToHeight($height) 
	{
		$ratio = $height / $this->getHeight();
		
		$width = $this->getWidth() * $ratio;
		
		$this->resize($width, $height);
	}
	
	function resizeToWidth($width) 
	{
		$ratio = $width / $this->getWidth();
		
		$height = $this->getheight() * $ratio;
		
		$this->resize($width, $height);
	}
	
	function scale($scale) 
	{
		$width = $this->getWidth() * $scale / 100;
		
		$height = $this->getheight() * $scale / 100; 
		
		$this->resize($width, $height);
	}
	
	function resize($width, $height) 
	{
		$newImage = imagecreatetruecolor($width, $height);
		
		imagecopyresampled($newImage, $this->imageResource, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
		
		$this->imageResource = $newImage;
	}
	
	function cut($width, $height)
	{
		if($this->getWidth() > $width)
		{
			$this->resizeToWidth($width);
		}
		if($this->getHeight() > $height)
		{
			$this->resizeToHeight($height);
		}
	}
	
	function ratioResize($width, $height)
	{
		echo "ratioResize <br>";
	
		$imageWidth = imagesx($this->imageResource);
		
		$imageHeight = imagesy($this->imageResource);
		
		$cropSize = $imageWidth > $imageHeight ? $imageHeight : $imageWidth;
		
		$newImage = imagecreatetruecolor($cropSize, $cropSize);
		
		imagecopy($newImage, $this->imageResource, 0, 0, 0, 0, $cropSize, $cropSize);
		
		$this->imageResource = $newImage;	
		
		if($cropSize == $imageHeight)
		{
			$this->resizeToHeight($height);
		}
		else
		{
			$this->resizeToWidth($width);
		}
		
		$newImage = imagecreatetruecolor($width, $height);
		
		$xOffset = (imagesx($this->imageResource) - $width) / 2;
		
		$yOffset = (imagesy($this->imageResource) - $height) / 2;
		
		imagecopy($newImage, $this->imageResource, 0, 0, $xOffset, $yOffset, $width, $height);
		
		$this->imageResource = $newImage;
	}
	
	function crop()
	{
		$currentWidth = imagesx($this->imageResource);
		
		$currentHeight = imagesy($this->imageResource);
		
		if($currentWidth == $currentHeight)
		{
			$croppedWidth = $currentWidth;
			
			$croppedHeight = $currentHeight;
		}
		if($currentWidth < $currentHeight)
		{
			$croppedWidth = $currentWidth;
			
			$croppedHeight = $currentWidth;
		}
		if($currentHeight< $currentWidth)
		{
			$croppedWidth = $currentHeight;
			
			$croppedHeight = $currentHeight;
		}
		
		$newImage = imagecreatetruecolor($croppedWidth, $croppedHeight);
		
		imagecopy($newImage, $this->imageResource, 0, 0, 0, 0, $croppedWidth, $croppedHeight);
		
		$this->imageResource = $newImage;
	}
	
	function cloneImage()
	{
		$cloneImage = imagecreatetruecolor($this->getWidth(), $this->getHeight());

		imagecopy($cloneImage, $this->imageResource, 0, 0, 0, 0, $this->getWidth(), $this->getHeight());

		$clone = new Application_Image();
		
		$clone->imageResource = $cloneImage;
		
		$clone->imageType = $this->imageType;
		
		return $clone;
	}
}
