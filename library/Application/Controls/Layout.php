<?php

class Application_Controls_Layout
{
	private $_boxes = Array();
	
	private $_controls = Array();

	private $_rootNode;
	
	public function setBoxes($boxes)
	{
		$this->_boxes = $boxes;
	}
	
	public function setControls($controlRows)
	{
		foreach($controlRows as $controlRow)
		{
			$control = Application_Controls_ControlsFactory::createFromRow($controlRow);
			
			$controlBoxLocalId = $control->getBoxLocalId();
			
			$this->_controls[$controlBoxLocalId] = $control;
		}
	}
	
	public function render()
	{
		if(!count($this->_boxes))
		{
			echo "No boxes";
			exit;
		}
		
		$insertedBoxes = Array();
		
		$_rootNode = new Application_Controls_VerticalWrapper($this->_boxes[0]);
		
		$insertedBoxes[1] = $_rootNode;

		
		for($i = 1; $i < count($this->_boxes); $i++)
		{
			$box = $this->_boxes[$i];
			
			$boxType = $box["box_type"];
			
			if($boxType == "Box")
			{
				$tableBox = new Application_Controls_TableBox($box);

				$localId = $tableBox->getLocalId();

				$tableBox->setControl($this->_controls[$localId]);

				$localParentId = $tableBox->getLocalParentId();
				
				$insertedBoxes[$localParentId]->addNode($tableBox);
				
				$insertedBoxes[$localId] = $tableBox;
			}
			if($boxType == "VerticalWrapper")
			{
				$verticalNode = new Application_Controls_VerticalWrapper($box);
				
				$localParentId = $verticalNode->getLocalParentId();
				
				$insertedBoxes[$localParentId]->addNode($verticalNode);
		
				$localId = $verticalNode->getLocalId();
		
				$insertedBoxes[$localId] = $verticalNode;
			}
			if($boxType == "HorizontalWrapper")
			{
				$horizontalNode = new Application_Controls_HorizontalWrapper($box);
				
				$localParentId = $horizontalNode->getLocalParentId();
				
				$insertedBoxes[$localParentId]->addNode($horizontalNode);
		
				$localId = $horizontalNode->getLocalId();
		
				$insertedBoxes[$localId] = $horizontalNode;
			}
		}
		
		echo $_rootNode->render();
		
	}
}

?>