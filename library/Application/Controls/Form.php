<?php

class Application_Controls_Form
{
	private $_type = "Form";

	private $_properties;
	
	private $_propertiesJSON;
	
	private $_pageParams;
	
	
	private $_item;
		
	private $_parentItem;
		
	private $_parentKeyName;
	
	private $_parentKeyValue;	
	
	
	private $_defaultItem = "";
	
	private $_defaultParentItem = "";
	
	private $_defaultParentKeyName = "";
	
	private $_defaultParentKeyValue = "";	
	
	
	public function __construct($propertiesJSON, $pageParams)
	{
		$this->_propertiesJSON = $propertiesJSON;
	
		$this->_pageParams = $pageParams;
	
		$this->_properties = json_decode($propertiesJSON);
		
		
		$this->_item = isSet($this->_properties->item) ? $this->_properties->item : $this->_defaultItem;
		
		$this->_parentItem = isSet($this->_properties->parentItem) ? $this->_properties->parentItem : $this->_defaultParentItem;
		
		$this->_parentKeyName = isSet($this->_properties->parentKeyName) ? $this->_properties->parentKeyName : $this->_defaultParentKeyName;
		
		$this->_parentKeyValue = isSet($this->_properties->parentKeyValue) ? $this->_properties->parentKeyValue : $this->_defaultParentKeyValue;
	}
	
	public function render()
	{	
		$item = Application_Controls_DataProvider::GetItem($this->_item);
		
		if($item)
		{
			$itemId = $item["item_id"];
		
			$itemFields = Application_Controls_DataProvider::GetItemFields($itemId);
			
			$html = "";
			
			$html .= "<form method='post' action='/item-instance/add-instance-execute'>";
			
			$html .= "<input type='hidden' name='itemId' value='$itemId'/>";
			
			
			if($this->_parentKeyName)
			{
				$parentKeyValue = isSet($this->_pageParams[$this->_parentKeyName]) ? $this->_pageParams[$this->_parentKeyName] : $this->_parentKeyValue;
			
				$html .= "<input type='hidden' name='parentId' value='$parentKeyValue'/>"; 
			}

			if($this->_parentItem)
			{
				$parentItem = Application_Controls_DataProvider::GetItem($this->_parentItem);
			
				$parentItemId = $parentItem["item_id"];
			
				$html .= "<input type='hidden' name='parentItem' value='$parentItemId'/>"; 
			}
			
			foreach($itemFields as $itemField)
			{
				$fieldType = $itemField["field_type"];
				
				$fieldKey = $itemField["field_key"];
			
				if($itemField["field_type"] == "textarea")
				{
					$html .= "<label>" . $itemField["field_name"] . "</label>";
					$html .= "<textarea name='key_$fieldKey'></textarea>";
				}
				if($itemField["field_type"] == "textinput")
				{
					$html .= "<label>" . $itemField["field_name"] . "</label>";
					$html .= "<input type='text' name='key_$fieldKey'></textarea>";
				}
				if($itemField["field_type"] == "select")
				{
					$html .= "<label>" . $itemField["field_name"] . "</label>";
					$html .= "<select></select>";
				}
			}
			
			$html .= "<input type='submit' value='zapisz'/>";
			
			$html .= "</form>";
			
			return $html;
		}
		else
		{
			return "unknown itemtype";
		}
	
		$keyValue = isSet($this->_pageParams[$this->_keyName]) ? $this->_pageParams[$this->_keyName] : $this->_keyValue;
		
		$item = Application_Controls_DataProvider::GetItem($this->_dataSource, $this->_keyName, $keyValue);
	
		$html = Application_Controls_FormatUtil::FillTemplate($this->_template, $item);
		
		return $html;
	}
	
	public function getPropertiesJSON()
	{
		return json_encode($this->getProperties());
	}
	
	public function getType()
	{
		return $this->_type;
	}
	
	public function getProperties()
	{
		$properties = Array();
		
		$properties["item"] = $this->_item;
		
		$properties["parentItem"] = $this->_parentItem;
		
		$properties["parentKeyName"] = $this->_parentKeyName;
		
		$properties["parentKeyValue"] = $this->_parentKeyValue;		
			
		return $properties;
	}
	
	public function getPropertiesMap()
	{
		$propertiesMap = Array();
		
		$propertiesMap["item"] = Array();
		
		$propertiesMap["item"]["type"] = "TEXTINPUT";
		
		$propertiesMap["parentItem"] = Array();
		
		$propertiesMap["parentItem"]["type"] = "TEXTINPUT";
		
		$propertiesMap["parentKeyName"] = Array();
		
		$propertiesMap["parentKeyName"]["type"] = "TEXTINPUT";
		
		$propertiesMap["parentKeyValue"] = Array();
		
		$propertiesMap["parentKeyValue"]["type"] = "TEXTINPUT";		
		
		return $propertiesMap;
	}
	
}

?>