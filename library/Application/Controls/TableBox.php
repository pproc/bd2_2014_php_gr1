<?php

class Application_Controls_TableBox
{
	private $_boxData;

	private $_localId;
	
	private $_localParentId;
	
	public function __construct($boxData)
	{
		$this->_boxData = $boxData;
		
		$this->_localId = $boxData["box_local_id"];
		
		$this->_localParentId = $boxData["box_local_parent_id"];
	}
	
	public function render()
	{
		$result = "";

		$result .= "<table class='tableNode' cellpadding='0' cellspacing='0' style='width:100%;'>";
		$result .= "   <tr class='tableRow'>";
		$result .= "       <td>";
		if ($this->_control)
		{
			$result .= $this->_control->render();
		}
		$result .= "       </td>";
		$result .= "   </tr>";
		$result .= "</table>";

		return $result;
	}
	
	public function getLocalId()
	{
		return $this->_localId;
	}
	
	public function getLocalParentId()
	{
		return $this->_localParentId;
	}
	
	public function setControl($control)
	{
		$this->_control = $control;
	}
}

?>






