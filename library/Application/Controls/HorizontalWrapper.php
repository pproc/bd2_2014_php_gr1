<?php

class Application_Controls_HorizontalWrapper
{
	private $_boxData;

	private $_localId;
	
	private $_localParentId;
	
	private $_children;
	

	public function __construct($boxData)
	{
		$this->_children = Array();
	
		$this->_boxData = $boxData;
		
		$this->_localId = $boxData["box_local_id"];
		
		$this->_localParentId = $boxData["box_local_parent_id"];
	}

	public function render()
	{
		$result = "";
		
		$result .= "<table class='horizontalWrapper' cellpadding='0' cellspacing='0' style='width:100%;'>";
		$result .= "	<tr class='horizontalWrapperTr'>";
		
		foreach($this->_children as $child)
		{
			$result .= "<td class='horizontalWrapperTd'>";
			$result .= 		$child->render();
			$result .= "</td>";
		}

		$result .= "	</tr>";
		$result .= "</table>";
		
		return $result;
	}
	
	public function addNode($node)
	{
		array_push($this->_children, $node);
	}
	
	public function getLocalId()
	{
		return $this->_localId;
	}
	
	public function getLocalParentId()
	{
		return $this->_localParentId;
	}	
}

?>