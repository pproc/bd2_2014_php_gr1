<?php

class Application_Controls_LoginForm
{
	private $_type = "LoginForm";

	private $_properties;
	
	private $_propertiesJSON;
	
	private $_pageParams;
	
	
	private $_item;
		
	private $_parentItem;
		
	private $_parentKeyName;
	
	private $_parentKeyValue;	
	
	
	private $_defaultItem = "";
	
	private $_defaultParentItem = "";
	
	private $_defaultParentKeyName = "";
	
	private $_defaultParentKeyValue = "";	
	
	
	public function __construct($propertiesJSON, $pageParams)
	{
		$this->_propertiesJSON = $propertiesJSON;
	
		$this->_pageParams = $pageParams;
	
		$this->_properties = json_decode($propertiesJSON);
		
		
		$this->_item = isSet($this->_properties->item) ? $this->_properties->item : $this->_defaultItem;
		
		$this->_parentItem = isSet($this->_properties->parentItem) ? $this->_properties->parentItem : $this->_defaultParentItem;
		
		$this->_parentKeyName = isSet($this->_properties->parentKeyName) ? $this->_properties->parentKeyName : $this->_defaultParentKeyName;
		
		$this->_parentKeyValue = isSet($this->_properties->parentKeyValue) ? $this->_properties->parentKeyValue : $this->_defaultParentKeyValue;
	}
	
	public function render()
	{	
	
		$user = Application_SessionUtil::getUser();
		
		
		if($user == null)
		{
			$html = "";  
			
			$html .= "<form method='post' action='/user/login-execute'>";
			
			$html .= "<label style='float:left;'>login:</label> <input type='text' name='userLogin' value='' style='float:left;'/>";
			
			$html .= "<label style='float:left;'>password:</label> <input type='text' name='userPassword' value='' style='float:left;'/>";
			
			$html .= "<input type='submit' value='ok' style='float:left;'/><br/>";
			
			$html .= "</form>";
			
			$html .= "<div style='clear:both;'></div>";
		}
		else
		{
			$html = "";
			
			$html .= "zalogowany jako: " . $user->user_email . " <a href='/user/logout-execute'>[wyloguj]</a>";
		}
	
		return $html;
	}
	
	public function getPropertiesJSON()
	{
		return json_encode($this->getProperties());
	}
	
	public function getType()
	{
		return $this->_type;
	}
	
	public function getProperties()
	{
		$properties = Array();
		
		$properties["item"] = $this->_item;
		
		$properties["parentItem"] = $this->_parentItem;
		
		$properties["parentKeyName"] = $this->_parentKeyName;
		
		$properties["parentKeyValue"] = $this->_parentKeyValue;		
			
		return $properties;
	}
	
	public function getPropertiesMap()
	{
		$propertiesMap = Array();
		
		$propertiesMap["item"] = Array();
		
		$propertiesMap["item"]["type"] = "TEXTINPUT";
		
		$propertiesMap["parentItem"] = Array();
		
		$propertiesMap["parentItem"]["type"] = "TEXTINPUT";
		
		$propertiesMap["parentKeyName"] = Array();
		
		$propertiesMap["parentKeyName"]["type"] = "TEXTINPUT";
		
		$propertiesMap["parentKeyValue"] = Array();
		
		$propertiesMap["parentKeyValue"]["type"] = "TEXTINPUT";		
		
		return $propertiesMap;
	}
	
}

?>