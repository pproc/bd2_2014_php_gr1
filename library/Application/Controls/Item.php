<?php

class Application_Controls_Item
{
	private $_type = "Item";

	private $_properties;
	
	private $_propertiesJSON;
	
	private $_pageParams;
	
	private $_applicationId = 1;
	
	
	private $_template;
	
	private $_dataSource;	
	
	private $_keyName;
	
	private $_keyValue;
	
	

	private $_defaultTemplate = "<div style='border:1px solid black; width: 100%; height:80px; background-color: pink;'></div>";
	
	private $_defaultDataSource = "articles";	
	
	private $_defaultKeyName = "id";
	
	private $_defaultKeyValue = "";
	
	
	private $_dataSetModel;
	
	
	public function __construct($propertiesJSON, $pageParams)
	{
		$this->_propertiesJSON = $propertiesJSON;
	
		$this->_pageParams = $pageParams;
	
		$this->_properties = json_decode($propertiesJSON);
		
		
		$this->_template = isSet($this->_properties->template) ? $this->_properties->template : $this->_defaultTemplate;
				
		$this->_dataSource = isSet($this->_properties->dataSource) ? $this->_properties->dataSource : $this->_defaultDataSource;		
		
		$this->_keyName = isSet($this->_properties->keyName) ? $this->_properties->keyName : $this->_defaultKeyName;
		
		$this->_keyValue = isSet($this->_properties->keyValue) ? $this->_properties->keyValue : $this->_defaultKeyValue;
		
		
		$this->_dataSetModel = new Application_Model_DataSet();
	}
	
	public function render()
	{	
		$dataSet = $this->_dataSetModel->getDataSetByKey($this->_applicationId, $this->_dataSource);
	
		$keyValue = $this->_keyValue ? $this->_keyValue : (isSet($this->_pageParams[$this->_keyName]) ? $this->_pageParams[$this->_keyName] : null);
			
		$item = $dataSet ? $this->_dataSetModel->getDataSetInstance($this->_applicationId, $dataSet, $this->_pageParams, $keyValue) : null;
				
		$html = Application_Controls_FormatUtil::FillTemplate($this->_template, $item);
		
		return $html;
	}
	
	public function getPropertiesJSON()
	{
		return json_encode($this->getProperties());
	}
	
	public function getType()
	{
		return $this->_type;
	}
	
	public function getProperties()
	{
		$properties = Array();
		
		$properties["template"] = $this->_template;
	
		$properties["dataSource"] = $this->_dataSource;
		
		$properties["keyName"] = $this->_keyName;
		
		$properties["keyValue"] = $this->_keyValue;
		
		return $properties;
	}
	
	public function getPropertiesMap()
	{
		$propertiesMap = Array();
		
		$propertiesMap["template"] = Array();
		
		$propertiesMap["template"]["type"] = "TEXTAREA";
		
		$propertiesMap["dataSource"] = Array();
		
		$propertiesMap["dataSource"]["type"] = "TEXTINPUT";
		
		$propertiesMap["keyName"] = Array();
		
		$propertiesMap["keyName"]["type"] = "TEXTINPUT";
		
		$propertiesMap["keyValue"] = Array();
		
		$propertiesMap["keyValue"]["type"] = "TEXTINPUT";
		
		return $propertiesMap;
	}
	
}

?>