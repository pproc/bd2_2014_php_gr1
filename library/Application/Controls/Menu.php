<?php

class Application_Controls_Menu
{
	private $_type = "Menu";

	private $_properties;
	
	private $_propertiesJSON;
	
	private $_pageParams;
	
	
	private $_menuId;
	
	private $_itemTemplate;
	
	
	public function __construct($propertiesJSON, $pageParams)
	{
		$this->_propertiesJSON = $propertiesJSON;
	
		$this->_pageParams = $pageParams;
	
		$this->_properties = json_decode($propertiesJSON);
		
				
		$this->_menuId = isSet($this->_properties->menuId) ? $this->_properties->menuId : null;
	}
	
	public function render()
	{	
		$menuPages = Application_Controls_DataProvider::GetMenu($this->_menuId);
	
		$result = '';
		
		$result .= '<div style="border:1px solid green; width: 100%; height: 100px;">';
		
		if($menuPages)
		{
			foreach($menuPages as $menuPage)
			{
				$result .= "<div style='float:left; padding:6px;'>";
				$result .= "<a href=/" . $menuPage["page_url"] . ">" . $menuPage["page_title"] . "</a>";
				$result .= "</div>";
			}
		}
		
		$result .= '</div>';
		
		return $result;
	}
	
	public function getPropertiesJSON()
	{
		return json_encode($this->getProperties());
	}
	
	public function getType()
	{
		return $this->_type;
	}	
	
	public function getProperties()
	{
		$properties = Array();
				
		$properties["menuId"] = $this->_menuId;
				
		$properties["itemTemplate"] = $this->_itemTemplate;

		return $properties;
	}
	
	public function getPropertiesMap()
	{
		$propertiesMap = Array();
		
		$propertiesMap["menuId"] = Array();
		
		$propertiesMap["menuId"]["type"] = "TEXTINPUT";
		
		$propertiesMap["itemTemplate"] = Array();
		
		$propertiesMap["itemTemplate"]["type"] = "TEXTAREA";
		
		return $propertiesMap;
	}
}

?>