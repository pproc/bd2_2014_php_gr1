<?php

class Controls_BoxWrapper
{
	private $_boxData;

	private $_boxLocalId;
	
	protected $_children = Array();
	
	public Controls_VerticalWrapper($boxData)
	{
		$this->_boxData = $boxData;
		
		$this->_boxLocalId = $boxData["box_local_id"];
	}

	public render()
	{
		$result = "";
		
		$result .= "<table class='verticalWrapper' cellpadding='0' cellspacing='0' style='width:100%;'>";

		foreach($this->_children as $child)
		{
			$result .= "<tr class='verticalWrapperTr'>";
			$result .= "	<td class='verticalWrapperTr'>";
			$result .= 			$this->_children.render();
			$result .= "	</td>";
			$result .= "</tr>";
		}

		$result .= "</table>";
	}
}

?>