<?php

class Application_Controls_PlaceHolder
{
	private $_boxLocalId;
	
	private $_controlJSON;
	
	private $_params;
	
	private $_width;
	
	private $_height;
	
	private $_id;
	
	private $_mode;
	
	private $_pageId;
	
	private $_applicationId;
	
	
	private $_defaultHeight = "auto";
	
	public function __construct($params)
	{
		$this->_params = $params;
		
		$this->_width = $params["width"];
		
		$this->_height = isSet($params["height"]) ? $params["height"] : $this->_defaultHeight;
		
		$this->_id = "placeHolder_" . $params["id"];
		
		$this->_mode = $params["mode"];
		
		$this->_pageId = $params["pageId"];
		
		$this->_applicationId = $params["applicationId"];
		
		$this->_pageParams = $params["pageParams"];
	}
	
	public function setBoxLocalId($boxLocalId)
	{
		$this->_boxLocalId = $boxLocalId;
	}
	
	public function getBoxLocalId()
	{
		return $this->_boxLocalId;
	}
	
	public function render()
	{
		$control = $this->loadControl();
	
		$result = '';
		
		$result .= '<div class="placeHolder" style="border: 1px dashed blue; width: ' . $this->_width . '; height: ' . $this->_height . ';" id="' . $this->_id . '">';
		
		if($this->_mode == "edit")
		{
			$controlPropertiesJSON = isSet($control) ? $control->getPropertiesJSON() : "";
			
			$controlPropertiesJSON = htmlspecialchars(stripslashes($controlPropertiesJSON));
			
			$controlType = isSet($control) ? $control->getType() : "";
			
			$result .= "<input type='hidden' class='controlPropertiesJSONInput' value=\"$controlPropertiesJSON\"/>";
			$result .= "<input type='hidden' class='controlTypeInput' value='$controlType'/>";
		}
		if($this->_mode == "view")
		{
			$result .= isSet($control) ? $control->render() : "";
		}
				
		$result .= '</div>';
		
		return $result;
	}
	
	public function loadControl()
	{
		$dbAdapter = Zend_Db_Table::getDefaultAdapter();
		
		$applicationId = $this->_applicationId;
		
		$pageId = $this->_pageId;
		
		$placeholderId = $this->_id;
		
		$pageParams = $this->_pageParams;
		
		
		$query = "SELECT * FROM controls WHERE con_app_id = '$applicationId' AND con_page_id = '$pageId' AND con_placeholder_id = '$placeholderId'"; 
			
			
		$result = $dbAdapter->query($query);
		
		$controlRow = $result->fetch();
		
		$controlType = $controlRow["con_type"];
		
		$controlPropertiesJSON = $controlRow["con_properties_json"];
		
		$control = null;
		
		
		if($controlType == "HTML")
		{
			$control = new Application_Controls_HTML($controlPropertiesJSON, $pageParams);
		}
		if($controlType == "Menu")
		{
			$control = new Application_Controls_Menu($controlPropertiesJSON, $pageParams);
		}
		if($controlType == "List")
		{
			$control = new Application_Controls_List($controlPropertiesJSON, $pageParams);
		}
		if($controlType == "Item")
		{
			$control = new Application_Controls_Item($controlPropertiesJSON, $pageParams);
		}		
		if($controlType == "Form")
		{
			$control = new Application_Controls_Form($controlPropertiesJSON, $pageParams);
		}	
		if($controlType == "SlidingList")
		{
			$control = new Application_Controls_SlidingList($controlPropertiesJSON, $pageParams);
		}			
		
		return $control;
	}
}

?>