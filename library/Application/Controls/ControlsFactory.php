<?php

class Application_Controls_ControlsFactory
{
	public static function createFromRow($controlRow)
	{
		$controlType = $controlRow["con_type"];
		
		$controlBoxLocalId = $controlRow["con_box_local_id"];
		
		$controlJSON = $controlRow["con_json"];
		
		if($controlType == "HTML")
		{
			$control = new Application_Controls_HTML($controlJSON);
		}
		if($controlType == "Menu")
		{
			$control = new Application_Controls_Menu($controlJSON);
		}
		
		$control->setBoxLocalId($controlBoxLocalId);
		
		return $control;
	}
}

?>