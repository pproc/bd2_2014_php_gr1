<?php

class Application_Controls_List
{
	private $_type = "List";

	private $_properties;
	
	private $_propertiesJSON;
	
	private $_pageParams;
	
	private $_applicationId = 1;
	
	
	private $_itemTemplate;
	
	private $_listSize;
	
	private $_dataSource;
	
	private $_cssClass;
	

	
	private $_defaultItemTemplate = "<div style='border:1px solid black; width: 100%;'></div>";
	
	private $_defaultListSize = 3;
	
	private $_defaultDataSource = "articles";
	
	private $_defaultCssClass = "list";
	
	
	private $_dataSetModel;
	
		
	public function __construct($propertiesJSON, $pageParams)
	{
		$this->_propertiesJSON = $propertiesJSON;
	
		$this->_pageParams = $pageParams;
	
		$this->_properties = json_decode($propertiesJSON);
		
		
		$this->_itemTemplate = isSet($this->_properties->itemTemplate) ? $this->_properties->itemTemplate : $this->_defaultItemTemplate;
		
		$this->_listSize = isSet($this->_properties->listSize) ? $this->_properties->listSize : $this->_defaultListSize;
		
		$this->_dataSource = isSet($this->_properties->dataSource) ? $this->_properties->dataSource : $this->_defaultDataSource;
		
		$this->_cssClass = isSet($this->_properties->cssClass) ? $this->_properties->cssClass : $this->_defaultCssClass;
		
		
		$this->_dataSetModel = new Application_Model_DataSet();
	}
	
	public function render()
	{		
		$dataSet = $this->_dataSetModel->getDataSetByKey($this->_applicationId, $this->_dataSource);

		if(!$dataSet)
		{
			return "UNKNOWN DATASET";
		}
		
		$itemInstances = $dataSet ? $this->_dataSetModel->getDataSetInstances($this->_applicationId, $dataSet, $this->_pageParams) : Array();
			
		$html = "";
		
		$html .= "<div class='" . $this->_cssClass . "'>";
		
		$index = 1;
		
		foreach($itemInstances as $itemInstance)
		{
			if($index > $this->_listSize)
			{
				break;
			}
		
			$html .= Application_Controls_FormatUtil::FillTemplate($this->_itemTemplate, $itemInstance);
							
			$index++;
		}
		
		$html .= "</div>";
		
		return $html;
	}
	
	public function getPropertiesJSON()
	{
		return json_encode($this->getProperties());
	}
	
	public function getType()
	{
		return $this->_type;
	}	
	
	public function getProperties()
	{
		$properties = Array();
		
		$properties["itemTemplate"] = $this->_itemTemplate;
		
		$properties["listSize"] = $this->_listSize;
		
		$properties["dataSource"] = $this->_dataSource;
		
		$properties["cssClass"] = $this->_cssClass;
		
		return $properties;
	}
	
	public function getPropertiesMap()
	{
		$propertiesMap = Array();
		
		$propertiesMap["itemTemplate"] = Array();
		
		$propertiesMap["itemTemplate"]["type"] = "TEXTAREA";
		
		$propertiesMap["listSize"] = Array();
		
		$propertiesMap["listSize"]["type"] = "TEXTINPUT";
		
		$propertiesMap["dataSource"] = Array();
		
		$propertiesMap["dataSource"]["type"] = "TEXTINPUT";

		$propertiesMap["cssClass"] = Array();
		
		$propertiesMap["cssClass"]["type"] = "TEXTINPUT";
		
		return $propertiesMap;
	}
}

?>