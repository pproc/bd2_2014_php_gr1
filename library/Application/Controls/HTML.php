<?php

class Application_Controls_HTML
{
	private $_type = "HTML";

	private $_properties;
	
	private $_propertiesJSON;
	
	private $_pageParams;
	
	
	private $_content;
	
	private $_cssClass;
	
	
	private $_defaultContent = "<p>DEFAULT CONTENT</p>";
	
	private $_defaultCssClass = "";

	
	public function __construct($propertiesJSON, $pageParams)
	{
		$this->_propertiesJSON = $propertiesJSON;
	
		$this->_pageParams = $pageParams;
	
		$this->_properties = json_decode($propertiesJSON);
		
		
		$this->_content = isSet($this->_properties->content) ? $this->_properties->content : $this->_defaultContent;
		
		$this->_cssClass = isSet($this->_properties->cssClass) ? $this->_properties->cssClass : $this->_defaultCssClass;
	}
	
	public function render()
	{
		$html = "";
		
		$html .= "<div class='" . $this->_cssClass . "'>";
	
		$html .= $this->_content;
		
		$html .= "</div>";
		
		return $html;
	}
	
	public function getPropertiesJSON()
	{
		return json_encode($this->getProperties());
	}
	
	public function getType()
	{
		return $this->_type;
	}
	
	public function getProperties()
	{
		$properties = Array();
		
		$properties["content"] = $this->_content;
		
		$properties["cssClass"] = $this->_cssClass;
		
		return $properties;
	}
	
	public function getPropertiesMap()
	{
		$propertiesMap = Array();
		
		$propertiesMap["content"] = Array();
		
		$propertiesMap["content"]["type"] = "TEXTAREA";

		$propertiesMap["cssClass"] = Array();
		
		$propertiesMap["cssClass"]["type"] = "TEXTINPUT";
		
		return $propertiesMap;
	}
}

?>