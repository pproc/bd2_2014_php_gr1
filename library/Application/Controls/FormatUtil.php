<?php

class Application_Controls_FormatUtil
{
	public static function FillTemplate($templateHTML, $itemRow)
	{
		$variableMatches = Array();
		
		preg_match_all('/\[\[([a-zA-Z0-9_ ]+)\]\]/', $templateHTML, $variableMatches);
				
		if(isSet($variableMatches[1]))
		{
			foreach($variableMatches[1] as $key => $variableMatch)
			{
				$variableValue = isSet($itemRow[$variableMatch]) ? $itemRow[$variableMatch] : "---";
			
				$templateHTML = str_replace($variableMatches[0][$key], $variableValue, $templateHTML);
			}
		}

		return $templateHTML;
	}
}