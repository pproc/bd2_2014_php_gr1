<?php

class Application_Controls_DataProvider
{
	static public function GetItemList($dataKey, $params)
	{
		$itemModel = new Application_Model_Item();
		
		$dataSetModel = new Application_Model_DataSet();
		
		if($dataSet = $dataSetModel->getDataSetByKey(1, $dataKey))
		{
			$itemId = $dataSet["ds_item_id"];
			
			$itemInstancesIds = $itemModel->getDataSetInstancesIds(1, $dataSet, $params);
		
			$instancesValues = $itemModel->getItemInstancesValues(1, $itemInstancesIds);			

			
			$instances = $itemModel->getDataSetInstances(1, $itemInstancesIds);
			
			$itemInstances = Array();
			
			foreach($instances as $instance)
			{
				$instanceId = $instance["inst_id"];
			
				$itemInstances[$instanceId] = $instance;
			}
			
			foreach($instancesValues as $instanceValue)
			{
				$valueInstanceId = $instanceValue["value_inst_id"];
				
				$valueFieldKey = $instanceValue["field_key"];
				
				$valueData = $instanceValue["value_data"];
				
				if(!isSet($itemInstances[$valueInstanceId]))
				{
					$itemInstances[$valueInstanceId] = Array();
				}
				
				$itemInstances[$valueInstanceId][$valueFieldKey] = $valueData;
			}			
			
			return $itemInstances;
		}
		else
		{
			return Array();
		}
	}
	
	static public function GetItem($itemName)
	{
		$itemModel = new Application_Model_Item();
		
		$item = $itemModel->getItemByName(1, $itemName);
		
		return $item;
	}
	
	static public function GetItemFields($itemId)
	{
		$itemModel = new Application_Model_Item();
		
		$itemFields = $itemModel->getItemFields(1, $itemId);
		
		return $itemFields;
	}
	
	static public function GetItemByName($itemName)
	{
		$itemModel = new Application_Model_Item();
		
		$item = $itemModel->getItemByName(1, $itemName);
		
		return $item;	
	}
	
	static public function GetItemInstance($dataKey, $keyName, $keyValue)
	{
		$itemModel = new Application_Model_Item();
		
		$dataSetModel = new Application_Model_DataSet();

		if($dataSet = $dataSetModel->getDataSetByKey(1, $dataKey))
		{
			$itemId = $dataSet["ds_item_id"];
			
			if($keyName == "id")
			{
				$instanceId = $keyValue;
				
				$instance = $itemModel->getItemInstance(1, $instanceId);

				$instanceValues = $itemModel->getItemInstanceValues(1, $instanceId);
			
				foreach($instanceValues as $instanceValue)
				{
					$fieldKey = $instanceValue["field_key"];
					
					$fieldValue = $instanceValue["value_data"];
					
					$instance[$fieldKey] = $fieldValue;
				}	
			
				return $instance;
			}
			
			return null;
		}
		else
		{
			return null;
		}
	}
	
	static public function GetMenu($menuId)
	{
		$user = Application_SessionUtil::getUser();
	
		$userRoleClause = "";
	
		if($user)
		{
			$userRoleId = $user->user_role_id;
			
			$userRoleClause = " OR page_role_id = '$userRoleId' ";
		}
	
		if($menuId)
		{
			$query = "SELECT * FROM pages WHERE page_in_menu = '1' AND (page_role_id = 0 $userRoleClause) ";
		
			$dbAdapter = Zend_Db_Table::getDefaultAdapter();
	
			$result = $dbAdapter->query($query);
	
			return $result->fetchAll();
		}
		else
		{
			return null;
		}
	}
	
	static public function GetRegisteredProviders()
	{
		
	}
}

?>