<?php

class Application_MyAuthAdapter extends Zend_Auth_Adapter_DbTable 
{
	protected $_additionalColumn = null;
	
	protected $_additionalId = false;


	public function setAdditionalColumn($columnName) 
	{
		$this->_additionalColumn = $columnName;
		
		return $this;
	}

	public function getAdditionalColumn() 
	{
		return $this->_additionalColumn !== false ? $this->_additionalColumn : '';
	}	
	
	public function setAdditionalId($id) 
	{
		$this->_additionalId = $id;
		
		return $this;
	}

	public function getAdditionalId() 
	{
		return $this->_additionalId !== false ? $this->_additionalId : '';
	}

	public function _authenticateCreateSelect() 
	{
		$select = parent::_authenticateCreateSelect();

		if($this->_additionalColumn)
		{
			$select->where($this->_zendDb->quoteIdentifier($this->_additionalColumn, true)." = ?", $this->getAdditionalId());
		}
		
		return $select;
	}
}

?>