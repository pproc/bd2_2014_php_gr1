<?php

class Application_MyController extends Zend_Controller_Action
{
	protected $_messageKey = null;
	
	protected $_userData = null;
	
	protected $_userId = 0;
	
	protected $_loggedUser = null;
	
	protected $_superadm = false;
		
	protected $_commonModel;
	
	protected $_params;
	
	protected $_config;
	
	

    public function init()
    {
		$this->_config = Zend_Registry::get('config');
		
		$this->_helper->layout()->setLayout('default');
		
		$this->_params = $this->_request->getParams();
		
		$this->setUserData();
		
		$this->setMessageData();
    }
	
	private function setMessageData()
	{
		$messageNamespace = new Zend_Session_Namespace("message");
		
		if(isSet($messageNamespace->key))
		{
			$messageKey = $messageNamespace->key;
			
			$messageMode = isSet($messageNamespace->mode) ? $messageNamespace->mode : true;
			
			$this->_messageKey = $messageKey;
			
			$configFile = "../application/configs/messages/pl.yml";
			
			$messages = new Zend_Config(Spyc::YAMLLoad($configFile));
			
			$this->view->controllerMessage = $messages->$messageKey;
			
			$this->view->controllerMessageMode = $messageMode;
			
			unset($messageNamespace->key);
		}
		else
		{
			$this->view->controllerMessage = null;
		}	
	}		
	
	private function setUserData()
	{
		if(Zend_Auth::getInstance()->hasIdentity())  
        {  	
			$this->_userData = Zend_Auth::getInstance()->getStorage()->read();

			$this->_userId = $this->_userData->user_id;
			
			$this->view->userData = $this->_userData;
			
			
			if($this->getRequest()->isPost() == false)
			{
				$userModel = new Application_Model_User();
			
				$loggedUser = $userModel->getLoggedUser($this->_userId);
				
				$this->_loggedUser = $loggedUser;
			}
		}
		
		$this->view->loggedUser = isSet($loggedUser) ? $loggedUser : null;
	}	
		
	protected function setProfileStats()
	{
		$profileModel = new Application_Model_Profile();
		
		$profileStats = $profileModel->getProfileStats($this->_userId);
		
		$this->view->profileStats = $profileStats;
	}
		
	protected function escape($messageKey, $target = null, $messageMode = true)
	{
		$messageNamespace = new Zend_Session_Namespace("message");
		
		$messageNamespace->key = $messageKey;
		
		$messageNamespace->mode = $messageMode;
		
		$url = $target ? $target : $_SERVER["HTTP_REFERER"];
		
		$this->_redirect($url);
	}
	
	protected function goBack($messageKey, $messageMode = true)
	{
		$messageNamespace = new Zend_Session_Namespace("message");
		
		$messageNamespace->key = $messageKey;
		
		$this->escape($messageKey, $_SERVER["HTTP_REFERER"], $messageMode);
	}	
	
	protected function formReturn()
	{
		$formNamespace = new Zend_Session_Namespace("form");
		
		$formNamespace->data = $this->_request->getPost();
		
		$this->goBack("VALIDATION_FAIL");
	}	
	
	protected function propagate($target)
	{
		$messageNamespace = new Zend_Session_Namespace("message");
		
		$messageNamespace->key = $this->_messageKey;
		
		$this->_redirect($target);
	}
	
	protected function isLogged()
	{
		if (isset($_SESSION['userAuth']))
			return true;
		else return false;
	}	
	
	protected function requireLogin()
	{
//		if($this->isLogged() == false)
//		{
//			$this->escape("LOGIN_REQUIRED", "/index");
//		}
	}

	
	protected function requireMaleGender()
	{
		if($this->_userData->user_gender == "0")
		{
			$this->escape("OPERATION_FAIL", "/");
		}
	}
	
	protected function requireFemaleGender()
	{
		if($this->_userData->user_gender == "1")
		{
			$this->escape("OPERATION_FAIL", "/");
		}
	}	

	protected function requireLogout()
	{
		if($this->isLogged() == true)
		{
			$this->escape("LOGOUT_REQUIRED", "/panel");
		}
	}
	
	protected function printAjaxResponse($value)
	{
		echo $value;
		
		exit;
	}

	protected function initEmailTransport()
	{
		//$config = array(
		//	'ssl' => $this->_config->mail_transport->ssl,
		//	'port' => $this->_config->mail_transport->port,
		//	'auth' => $this->_config->mail_transport->auth,
		//	'username' => $this->_config->mail_transport->username,
		//	'password' => $this->_config->mail_transport->password
		//	);

		//$mailTransport = new Zend_Mail_Transport_Smtp($this->_config->mail_transport->smtp, $config);
		
		//Zend_Mail::setDefaultTransport($mailTransport);
	}	
	
	protected function prepareSearch()
	{
		$searchId = null;
		
		if($this->_request->isPost())
		{
			$searchId = rand();
			
			$_SESSION['search_'.$searchId] = $this->_request->getPost();

			$this->escape(null, $_SERVER["REQUEST_URI"] . "/searchId/$searchId");
		
			return $searchId;
		}
		
		elseif($this->_hasParam('searchId'))
		{
			$searchId = $this->_request->getParam('searchId');
			$this->_params = array_merge($this->_params, $_SESSION['search_'.$searchId]);
		}
		
		if(!is_null($searchId))
		{
			$this->view->searchId = $searchId;
		}
		else
		{
			$this->view->searchId = "";
		}
		
		$this->_params["perPage"] = Application_Utils::getItemsPerPage($this->_params);
		
		$this->view->searchParams = $this->_params;
	}	
}