<?php

class Application_DbUtil
{
	/*
	Oszacowanie ilości jednostek na stronę w gridzie
	*/
	public static function getItemsPerPage($params)
	{
		if(isSet($params["perPage"]))
		{
			$itemsPerPage = $params["perPage"];
		}
		else
		{
			$config = Zend_Registry::get('config');
			$itemsPerPage = $config->editable->items_per_page;
		}
		return $itemsPerPage;
	}
	
	/*
	Oszacowanie aktualnej strony w gridzie
	*/
	public static function getCurrentPage($params)
	{
		$currentPageNumber = isSet($params["page"]) ? $params["page"] : 1;
		return $currentPageNumber;
	}
	
	/*
	Oszacowanie klucza sortowania w gridzie
	*/
	public static function getOrderClause($params)
	{
		$orderClause = null;
		
		if(isSet($params["order"]))
		{
			$tab = explode("_", $params["order"]);
			
			$direction = $tab[count($tab)-1];
			unset($tab[count($tab)-1]);
			$column = implode("_", $tab);
			
			$orderClause = "$column $direction";
		}
		return  $orderClause;
	}	
	
	/*
	Utworzenie standardowego paginatora
	*/
	public static function getPaginator($select, $params)
	{
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbTableSelect($select));
		$paginator->setCurrentPageNumber(Application_DbUtil::getCurrentPage($params));
		$paginator->setItemCountPerPage(Application_DbUtil::getItemsPerPage($params));
		
		try
		{
			$paginator->getPages();
			return $paginator;
		}
		catch(Exception $ex)
		{
			print_r($ex->getMessage());
			exit;
			return null;
		}
	}
	
}