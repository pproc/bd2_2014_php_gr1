<?php

class Application_Utils
{
	public static function getItemsPerPage($params)
	{
		if(isSet($params["perPage"]))
		{
			$itemsPerPage = $params["perPage"];
			
			$_SESSION["perPage"] = $itemsPerPage;
		}
		
		if(isSet($_SESSION["perPage"]))
		{
			$itemsPerPage = $_SESSION["perPage"];
		}	
		else
		{
			$config = Zend_Registry::get('config');
			
			$itemsPerPage = $config->editable->items_per_page;
		}
		
		return $itemsPerPage;
	}
	
	public static function getCurrentPage($params)
	{
		$currentPageNumber = isSet($params["page"]) ? $params["page"] : 1;
		
		return $currentPageNumber;
	}
	
	public static function getOrderClause($params)
	{
		$orderClause = null;
		
		if(isSet($params["order"]))
		{
			$tab = explode("_", $params["order"]);
			
			$direction = $tab[count($tab)-1];
			
			unset($tab[count($tab)-1]);
			
			$column = implode("_", $tab);
			
			$orderClause = "$column $direction";
		}
		else
		{
			$tab = explode("_", $params["defaultOrder"]);
			
			$direction = $tab[count($tab)-1];
			
			unset($tab[count($tab)-1]);
			
			$column = implode("_", $tab);
			
			$orderClause = "$column $direction";		
		}
		
		return  $orderClause;
	}

	public static function getPaginator($select, $params)
	{
		$paginatorAdapter = new Zend_Paginator_Adapter_DbTableSelect($select);
	
		$dbAdapter = Zend_Db_Table::getDefaultAdapter();
	
		$paginator = new Zend_Paginator($paginatorAdapter);
		
		$paginator->setCurrentPageNumber(Application_Utils::getCurrentPage($params));
		
		$paginator->setItemCountPerPage(Application_Utils::getItemsPerPage($params));
		 
		$paginator->getPages();
		
		return $paginator;
	}
	
	public static function throwFile($filePath, $fileName)
	{
		header($_SERVER["SERVER_PROTOCOL"]." 200 OK");
		
		header("Cache-Control: public");
		
		header("Content-Type: application/octet-stream");
		
		header("Content-Transfer-Encoding: Binary");
		
		header("Content-Length:".filesize($filePath));
		
		header("Content-Disposition: attachment; filename=\"$fileName\"");
		
		ob_clean();
		
		flush();				
		
		readfile($filePath);
		
		exit();		
	}	
	
	public static function throwImage($filePath, $fileName)
	{
		header($_SERVER["SERVER_PROTOCOL"]." 200 OK");
		
		header("Cache-Control: public");
		
		header("Content-Type: image");
		
		header("Content-Transfer-Encoding: Binary");
		
		header("Content-Length:".filesize($filePath));
		
		header("Content-Disposition: attachment; filename=\"$fileName\"");
		
		ob_clean();
		
		flush();				
		
		readfile($filePath);
		
		exit();		
	}	
}