<?php

class Application_MyModel
{
	protected $_db;
	
	public function __construct()
	{
		$this->_db 	   = Zend_Db_Table::getDefaultAdapter();
		$this->_logger = Application_Logger::getInstance();
	}
	
	public function beginTransaction()
	{
		$this->_db->query("BEGIN");
	}
	
	public function commitTransaction()
	{
		$this->_db->query("COMMIT");
	}
	
	public function rollbackTransaction()
	{
		$this->_db->query("ROLLBACK");
	}
	
}