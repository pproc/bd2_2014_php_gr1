<?php

class Application_SessionUtil
{
	public static function getUser()
	{
		if(Zend_Auth::getInstance()->hasIdentity())  
        {  	
			$user = Zend_Auth::getInstance()->getStorage()->read();

			return $user;
		}
		
		return null;
	}
}