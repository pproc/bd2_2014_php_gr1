<?php

require_once 'Zend/Validate/Abstract.php';

class Zend_Validate_MyPatterns_UserName extends Zend_Validate_Abstract
{
    public function isValid($value)
    {
		if(strlen($value) < 2 || strlen($value) > 60)
		{
			return false;
		}		
		
		return true;
		
		$pattern = "/^[ _\\-\\u0041-\\u005A\\u0061-\\u007A\\u00C0-\\u00D6\\u00D8-\\u00F6\\u00F8-\\u00FF\\u0100-\\u0148\\u014A-\\u017F\\u0180-\\u024F\\u0250-\\u02AF\\u0370-\\u03FF\\u0400-\\u04FF\\u0500-\\u052F\\u0530-\\u058F\\u0590-\\u05FF\\u0600-\\u06FF\\u0700-\\u074F0-9\\.]{2,60}$/";
	
		if(preg_match($pattern, $value))
		{
			return true;
		}
		else
		{
			return false;
		}
    }
}
