<?php

require_once 'Zend/Validate/Abstract.php';

class Zend_Validate_MyPatterns_PostCode extends Zend_Validate_Abstract
{
    public function isValid($value)
    {
		if(strlen($value) == 0)
		{
			return true;
		}
	
		$pattern = "/^[0-9]{2}-[0-9]{3}$/";
	
		if(preg_match($pattern, $value))
		{
			return true;
		}
		else
		{
			return false;
		}
    }
}
