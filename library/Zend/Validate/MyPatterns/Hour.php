<?php

require_once 'Zend/Validate/Abstract.php';

class Zend_Validate_MyPatterns_Hour extends Zend_Validate_Abstract
{
    public function isValid($value)
    {
		$pattern = "/^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/";
	
		if(preg_match($pattern, $value))
		{
			return true;
		}
		else
		{
			return false;
		}
    }
}
