<?php

require_once 'Zend/Validate/Abstract.php';

class Zend_Validate_MyPatterns_Name extends Zend_Validate_Abstract
{
    public function isValid($value)
    {
		if(strlen($value) < 2 || strlen($value) > 60)
		{
			return false;
		}		
		
		$pattern = "/^[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ,._'` -]{2,60}$/";
	
		if(preg_match($pattern, $value))
		{
			return true;
		}
		else
		{
			return false;
		}
    }
}
