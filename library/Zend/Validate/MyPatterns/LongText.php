<?php

require_once 'Zend/Validate/Abstract.php';

class Zend_Validate_MyPatterns_LongText extends Zend_Validate_Abstract
{
    public function isValid($value)
    {
		//$pattern = "/^.{1,12000}$/";
	
		if(strlen($value) >= 1 && strlen($value) < 12000)
		{
			return true;
		}
		else
		{
			return false;
		}
    }
}