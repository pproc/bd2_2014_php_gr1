<?php

require_once 'Zend/Validate/Abstract.php';

class Zend_Validate_MyPatterns_Password extends Zend_Validate_Abstract
{
    public function isValid($value)
    {
		if(strlen($value) < 6)
		{
			return false;
		}
		if(strlen($value) > 20)
		{
			return false;
		}
		
		return true;
    }
}
