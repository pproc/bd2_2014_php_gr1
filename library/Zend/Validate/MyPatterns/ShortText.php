<?php

require_once 'Zend/Validate/Abstract.php';

class Zend_Validate_MyPatterns_ShortText extends Zend_Validate_Abstract
{
    public function isValid($value)
    {
		//$pattern = "/^.{1,50}$/";
		
		if(strlen($value) >= 1 && strlen($value) < 50)
		{
			return true;
		}
		else
		{
			return false;
		}
    }
}
