<?php

require_once 'Zend/Validate/Abstract.php';

class Zend_Validate_MyPatterns_Login extends Zend_Validate_Abstract
{
    public function isValid($value)
    {
		if(strlen($value) < 4 || strlen($value) > 30)
		{
			return false;
		}
	
		$pattern = "/^[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ0-9._ -]*$/";
	
		if(preg_match($pattern, $value))
		{
			return true;
		}
		else
		{
			return false;
		}
    }
}
