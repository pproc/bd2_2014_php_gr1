<?php /* Smarty version Smarty-3.0.7, created on 2014-04-02 21:33:02
         compiled from "/var/www/application/layouts/default.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1539683256533c73fe984bb3-03778262%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8ad164ceee29cdb97ebc46a66bfd2a5834261220' => 
    array (
      0 => '/var/www/application/layouts/default.tpl',
      1 => 1387751713,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1539683256533c73fe984bb3-03778262',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html>
<html>

<head>
	<meta name="google-site-verification" content="sOreLOZaFGs_6FQOB4tf-3ZVlA_q8GAsqP9xcPDsYaE" />
	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	 
	
	<meta http-equiv="Expires" content="0" />
	
	<meta http-equiv="Pragma" content="no-cache" />
	
	<meta http-equiv="Cache-Control" content="no-cache" />
	
	<meta name="author" content="mkocik&mpilat" />
	
	<link href="/public/css/reveal.css" rel="stylesheet" type="text/css" />
	
	<link rel="stylesheet" href="http://cdn.sencha.io/try/extjs/4.0.7/resources/css/ext-all-gray.css" />
	   
	<link href="/public/css/pure_forms.css" rel="stylesheet" type="text/css" />
	<link href="/public/css/pure_buttons.css" rel="stylesheet" type="text/css" />
	
	<script src="/public/scripts/jquery.min.js" type="text/javascript"></script>
	
	
	<script src="/public/scripts/jquery.validate.js" type="text/javascript"></script>
	
	<script src="/public/scripts/jquery.validate.myrules.js" type="text/javascript"></script>
	
	<script src="/public/scripts/grids.js" type="text/javascript" ></script>
	
	<script src="/public/scripts/jquery.reveal.js" type="text/javascript" ></script>
	
	<script src="/public/bootstrap-3.0.3-dist/dist/js/bootstrap.min.js" type="text/javascript" ></script>
	<link rel="stylesheet" href="/public/bootstrap-3.0.3-dist/dist/css/bootstrap.min.css" type="text/css"/>
	<link rel="stylesheet" href="/public/bootstrap-3.0.3-dist/dist/css/bootstrap-theme.min.css" type="text/css"/>

	
	<title>MemoMail</title>


</head>

<body>
<?php if (isset($_smarty_tpl->getVariable('user',null,true,false)->value)){?>
            <div class="navbar navbar-inverse navbar-fixed-top" role="navigation" >
              
                    <nav class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li>
								<img src="/public/images/koperta12.png"/ alt="logo" style="float:left;margin-left:20px">

                            </li>    

							<li>
								<span class="navbar-brand" style="margin-top:5px;">
									Witaj <?php echo $_smarty_tpl->getVariable('user_email')->value;?>

								</span>
							</li>
                    	</ul>
						<ul class="nav navbar-nav navbar-right">
                            <li>
                                <a style="font-weight: bold;float: right;font-size:large;margin-top:5px;margin-right:25px;" id="LogOutLink" href="/index/log-out">WYLOGUJ <span class="glyphicon glyphicon-off"></span></a>
                            </li>
						</ul>
                    </nav>	
            </div>	


        <div class="container" style="font-size:x-large;margin-top:100px;margin-left:0px"> 
            <div class="row">
                    <div class="sidebar col-lg-3 col-sm-12">
                            <ul class="nav nav-pills nav-stacked">
                                <li class="adminCategory">
                                    <a href="/main-panel"><span class="glyphicon glyphicon-tasks"></span> Zadania </a>
                                </li>
                                <li class="adminCategory">
                                    <a href="/receivers"><span class="glyphicon glyphicon-envelope"></span> Odbiorcy </a>
                                </li>
								<li>
                                     <a href="/priorities"><span class="glyphicon glyphicon-exclamation-sign"></span> Priorytety </a>
                                </li>
                                <li class="adminCategory">
                                    <a href="/history"><span class="glyphicon glyphicon-list"></span> Historia </a>
                                </li>
                                <li class="adminCategory">
                                    <a href="/account"><span class="glyphicon glyphicon-user"></span> Moje konto </a>
                                </li>
                            </ul>                  
                    </div>
                <div  class="content col-lg-9 col-sm-12">     	
                    <?php echo $_smarty_tpl->getVariable('layout')->value->content;?>

                </div>
            </div>
        </div>


<?php }else{ ?>

<?php echo $_smarty_tpl->getVariable('layout')->value->content;?>


<?php }?>



	 
</body>

</html>

